
<%@page import="operations.Checks"%>
<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="java.util.List"%>
<div class="right">
	<div class="blog">
		<h2>
			<span>Sidebar</span> Menu
		</h2>
		<ul>
			<%
			String uname = session.getAttribute("uname") != null ? session
						.getAttribute("uname").toString() : "";
			String auname = session.getAttribute("auname") != null ? session
						.getAttribute("auname").toString() : "";					
			if(Checks.isEmpty(uname) && Checks.isEmpty(auname)){ %>
				<li><a href="home.jsp"><span>Home</span></a></li>
				<li><a href="donate.do"><span>Donate 10$</span></a></li>
			<%} else if(!Checks.isEmpty(uname)){ %>
			<li><a href="home.jsp"><span>Home</span></a></li>
			<li><a href="member/addthreadinfo.jsp">Add Thread</a></li>
			<li><a href="member/viewthreadinfo.do">My Threads</a></li>
			<li><a href="changepassword.jsp">Change Password</a></li>
			<li><a href="member/signout.jsp">Signout</a></li>
			<%}else if(!Checks.isEmpty(auname)){ %>
			<li><a href="home.jsp"><span>Home</span></a></li>
			<li><a href="admin/addcategoryinfo.jsp">Add Category Info</a></li>
			<li><a href="admin/viewcategoryinfo.do">view all category
					info details</a></li>
			<li><a href="changepassword.jsp">Change Password</a></li>
			<li><a href="admin/signout.jsp">Signout</a></li>
			<%} %>
		</ul>
	</div>

	<div class="blog">
		<h2>
			<span>Categories...</span>
		</h2>
		<ul class="category">
			<%		
		  	List<CategoryInfo> allcategories = CategoryInfoDAO.getallrecord();
		  	if(allcategories != null){
		  		for(CategoryInfo d : allcategories){		 			
					out.println("<li class=\"category\">");
						out.println("<a href=\"viewthread_category.jsp?categoryid="+ d.getCategoryid()+ "\">"+d.getCategoryname()+"</a><br />");
					out.println("</li>");
		  		}
		  	}
		
		%>
		</ul>
	</div>
	<div class="blog">
		<h2>Search</h2>
		<div class="search">
			<form id="form1" name="form1" method="post" action="searchresult.jsp">
				<span> <input name="searchtext" type="text" class="keywords"
					id="textfield" maxlength="50" placeholder="Search..."
					value="${param.searchtext }" />
				</span> <input name="b" type="image" src="images/search.gif" class="button" />
			</form>
		</div>
		<div class="clr"></div>
	</div>
</div>
