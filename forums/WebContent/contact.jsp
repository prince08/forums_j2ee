<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="operations.QuestionsList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="java.util.List"%>
<%@page import="operations.Checks"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1	

	String uname = session.getAttribute("uname") != null ? session
			.getAttribute("uname").toString() : "";
	String auname = session.getAttribute("auname") != null ? session
			.getAttribute("auname").toString() : "";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Contact us</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" type="text/css" href="reveal.css" />
<script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#ForgotPassForm").validate({
			rules : {
				username : {
					required : true,
				},
				sq : "required",
				sans : {
					required : true,
				}
			},
			messages : {
				username : {
					required : " Required",
				},
				sq : " Please select a Security question",
				sans : {
					required : " Required",
				}
			}
		});
	});
	$(document).ready(function() {
		$("#contForm").validate({
			rules : {
				name : "required",
				subject : "required",
				mess : "required",
				email:{
					required: true,
					email:true
				},
				contactno:{
					required:true,
					number:true
				}
			},
			messages : {
				name : " Please fill this field.",
				subject : " Please fill this field.",
				mess : " Please fill this field.",
				email:{
					required: "This Field is required.",
					email:"Please enter a valid emilID"
				},
				contactno:{
					required:"This field is required.",
					number:"Please enter a valid Contact number."
				}
			}
		});
	});
</script>
<script type="text/javascript">
	tinymce.init({
		selector : 'textarea',
		menubar : false
	});
</script>

</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<!-- xxxxxxxxxxxxxxxxxxx-----header----xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
		<div class="header">
			<div class="header_resize">
				<div class="logo">
					<h1>
						<a href="#">Forum<small>Where we answer to every
								problem...</small></a>
					</h1>
				</div>
				<div class="menu">
					<ul>
						<li><a href="home.jsp"><span>Home</span></a></li>
						<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
						<li><a href="contact.jsp" class="active"><span>
									Contact Us</span></a></li>
					</ul>
				</div>
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
			<div class="headert_text_resize">
				<div class="textarea">

					<%
				if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
			%>
					<h2>Login</h2>
					<form method="post" action="checklogin.do">
						<table>
							<tr>
								<td>UserName:</td>
								<td><input type="text" name="username"
									value="${param.username }" placeholder="Username" /></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password"
									placeholder="Password" /></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><input type="submit"
									name="login" value="LOGIN" /></td>

							</tr>
							<tr>
								<td align="center" colspan="2"><a href="#"
									data-reveal-id="myModal" data-animation="fade">Forgot
										password...</a></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><a href="addLoginInfo.jsp">Not
										having an account...Click here!!...</a></td>
							</tr>
						</table>
					</form>
					<%
				} else {
			%>
					<h3>Welcome ${auname }${uname }</h3>
					<%
				
			if(session.getAttribute("memberid")!=null){
				String mid = session.getAttribute("memberid").toString();
				List<MemberPics> pics = MemberPicsDAO.getallrecord(Integer.parseInt(mid));
				if(pics!=null && pics.size()>0){
					MemberPics pic = pics.get(0);
					String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
					out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
				}else{
				String path = getServletContext().getRealPath("/upload/d.jpeg");
				out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
				}
			}
			%>

					<%if(!Checks.isEmpty(uname)){ %>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="addmemberpic.jsp" />View/Upload
					Photos..</a>
					<%} %>
					<p>
						<%
				if (session.getAttribute("lastlogin") != null) {
						out.println("<br/><h3><span>Last Time You Visit : "
								+ session.getAttribute("lastlogin")
								+ "</span></h3>");
					} else {
						out.println("<h3><span>Welcome First Time in Our Site!!!</span></h3>");
					}

				}
			%>
					</p>
					<div>${message }</div>

				</div>
				<img src="images/img_11.jpg" alt="" width="631" height="279" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>
		<!-- xxxxxxxxxxxxxxxxxx---hedare end----xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

		<div class="body">
			<div class="body_resize">
				<div class="left">
					<%
					String message_contact = request.getAttribute("message_contact")!=null? request.getAttribute("message_contact").toString():"";
					if(!Checks.isEmpty(message_contact)){
					%>
					<h3>
						<span style="color: black;">${message_contact }</span>
					</h3>
					<br />
					<div class="bg"></div>
					<% }	%>
					<!-- main area where i write main code... -->
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<h3>
						<span>Contact Us</span>
					</h3>
					<form method="post" action="contact.do" id="contForm">
						<table cellpadding="5" cellspacing="5">
							<tr>
								<td>Name</td>
								<td><input type="text" id="name" placeholder="Name"
									name="name" value="${param.name }" /></td>
							</tr>
							<tr>
								<td>Subject</td>
								<td><input type="text" name="subject" placeholder="Subject"
									id="subject" value="${param.subject}" /></td>
							</tr>
							<tr>
								<td>Email ID</td>
								<td><input type="text" name="email" placeholder="Example@example.com"
									id="email" value="${param.email}" /></td>
							</tr>							
							<tr>
								<td>Contact Number</td>
								<td><input type="text" name="contactno" placeholder="Contact Number"
									id="contactno" value="${param.contactno}" /></td>
							</tr>
							
							<tr>
								<td>Your Message</td>
								<td><textarea name="mess" rows="5	" cols="22" id="mess"
										style="resize: none">${param.mess }</textarea></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Send Message" /></td>
							</tr>
						</table>
					</form>
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>

				<!-- sidebar------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				<jsp:include page="DivClassRight.jsp" />
				<!-- -----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--------------- -->
				<div class="clr"></div>
			</div>
		</div>
		<jsp:include page="footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->

	<!-- Forgot password with popup.. -->
	<jsp:include page="forgotpassword.jsp" />
	<!-- forgot pass form..end... -->
</body>
</html>