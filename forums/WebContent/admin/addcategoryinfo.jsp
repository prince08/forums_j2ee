<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	if (session.getAttribute("auname") == null) {
		response.sendRedirect("../home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/arial.js"></script>
<script type="text/javascript" src="../js/cuf_run.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector : 'textarea',
		menubar : false
	});
</script>

</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<jsp:include page="../header.jsp" />
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<%
						String categoryid = request.getParameter("categoryid") != null
								? request.getParameter("categoryid").trim()
								: "";
						String categoryname = request.getParameter("categoryname") != null
								? request.getParameter("categoryname").trim()
								: "";
						String description = request.getParameter("description") != null
								? request.getParameter("description").trim()
								: "";
					%>
					<form method="get" action="insertcategoryinfo.do">
						<h1 align="center">....Category Form.....</h1>
						<div class="bg"></div>
						<table cellpadding="10" cellspacing="10" align="center">
							<tr>
								<td>Enter Category Id:</td>
								<td><input type="text" name="categoryid"
									value=<%=categoryid%>></input></td>
							</tr>
							<tr>
								<td>Enter category name:</td>
								<td><input type="text" name="categoryname"
									value=<%=categoryname%>></input></td>
							</tr>
							<tr>
								<td>Enter description:</td>
								<td><textarea name="description" rows="5" cols="22"
										style="resize: none">${param.description } </textarea></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><input type="submit"
									name="sunmit" value="Submit category" /></td>
							</tr>
						</table>
					</form>
					<h2 align="center">${message_addcategory }</h2>

					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="../footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>