<%@page import="model.to.CategoryInfo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
    response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	if (session.getAttribute("auname") == null) {
		response.sendRedirect("../home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/arial.js"></script>
<script type="text/javascript" src="../js/cuf_run.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector : 'textarea',
		menubar : false
	});
</script>

</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<jsp:include page="../header.jsp" />
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<h1 align="center"><span style="color: orange;"></span>Category details</span></h1>
					<div class="bg"></div>
					<%
	String message = request.getParameter("message2")!=null?request.getParameter("message2"):"";
	if(request.getAttribute("allrecords") != null){
		if(request.getAttribute("allrecords") instanceof List<?>){
			List<?> allrecords = (List<?>)request.getAttribute("allrecords");
			Object c =request.getAttribute("categoryid")!=null?request.getAttribute("categoryid"):"";
			String categoryid1 = (String) c;
			for(Object d : allrecords)
			{
				if(d instanceof CategoryInfo)					
				{
					CategoryInfo data = (CategoryInfo) d;
					//System.out.println("hello");
					if(categoryid1.equals(data.getCategoryid())){					
						out.println("<form method=\"post\" action = \"updatecategoryinfo.do\">");
						out.println("<h3>Update Category</h3>");
						out.println("<div class=\"bg\"></div>");
						out.println("<table cellspacing=\"5\" cellpadding=\"5\" align=\"center\">");
						out.println("<tr>");
						out.println("<td>Category ID: </td><td><input  readonly=\"readonly\" type = \"text\" name = \"categoryid\" value = \""+data.getCategoryid()+"\"/></td>");
						out.println("</tr><tr>");	
						out.println("<td>Category Name: </td><td><input type = \"text\" name = \"categoryname\" value = \""+data.getCategoryname()+"\"/></td>");
						out.println("</tr><tr>");						
						out.println("<td>Category Description: </td><td><textarea name=\"description\" rows=\"5\" cols=\"22\" style=\"resize:none\">"+data.getDescription()+"</textarea></td>");
						out.println("</tr><tr>");
						out.println("<td colspan=\"2\" align = \"center\"><input type=\"submit\" value = \"Update\"/></td>");						
						out.println("</tr>");
						out.println("</table>");
						out.println("</form>");
						out.println("<div class=\"bg\"></div>");
					}else{		
						
					%>
					<h3>
						<span><%=data.getCategoryname() %></span>
					</h3>
					<h4>
						&nbsp;&nbsp;<%= data.getCategoryid() %></h4>
					<p><%=data.getDescription() %>
					</p>
					<br />
					<%						
						out.println("<a href=\"viewcategoryinfo.do?categoryid1="+ data.getCategoryid()+ "\">");
							out.println("<img src=\"images/edit_logo.png\" height=\"25px\" width=\"25px\" /></a>");
							
							out.println("<a onclick=\"return confirm('Are you sure to delete this record?')\" href=\"deletecategoryinfo.do?categoryid2="+ data.getCategoryid()+ "\">");
							out.println("<img src=\"images/del_logo.png\" height=\"25px\" width=\"25px\" /></a>");						
						%>
					<div class="bg"></div>
					<%																
					}				
				}
			}	
		}
	}
	else{
		out.println("<tr><td colspan=\"9\" align=\"center\">There is No Data</td></tr>");
	}
%>
					</table>
					<h1>${message_viewcategory }</h1>

					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="../footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>