<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="model.to.ThreadInfo"%>
<%@page import="model.dao.ThreadInfoDAO"%>
<%@page import="java.util.List"%>
<%@page import="operations.Checks"%>
<%@page import="operations.QuestionsList"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	if(request.getParameter("categoryid")==null){
		response.sendRedirect("home.jsp");
	}
String uname = session.getAttribute("uname") != null ? session
		.getAttribute("uname").toString() : "";
String auname = session.getAttribute("auname") != null ? session
		.getAttribute("auname").toString() : "";			
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" type="text/css" href="reveal.css" />
<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#ForgotPassForm").validate({
			rules:{				
				username:{
					required: true,					
				},				
				sq:"required",
				sans:{
					required: true,					
				}						
			},
			messages:{
				username:{
					required:" Required",					
				},
				sq: " Please select a Security question",
				sans:{
					required: " Required",									
				}				
			}
		});
	});
</script>
</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">

		<div class="header">
			<div class="header_resize">
				<div class="logo">
					<h1>
						<a href="home.jsp">Forum<small>Where we answer to
								every problem...</small></a>
					</h1>
				</div>
				<div class="menu">
					<ul>
						<li><a href="home.jsp" class="active"><span>Home</span></a></li>
<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
						<li><a href="contact.jsp"><span> Contact Us</span></a></li>
					</ul>
				</div>
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
			<div class="headert_text_resize">
				<div class="textarea">
					<%
				if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
					%>
					<h2>Login</h2>
					<form method="post" action="checklogin.do">
						<table>
							<tr>
								<td>UserName:</td>
								<td><input type="text" name="username"
									value="${param.username }" placeholder="Username" /></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password"
									placeholder="Password" /></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><input type="submit"
									name="login" value="LOGIN" /></td>

							</tr>
							<tr>
								<td align="center" colspan="2"><a href="#"
									data-reveal-id="myModal" data-animation="fade">Forgot
										password...</a></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><a href="addLoginInfo.jsp">Not
										having an account...Click here!!...</a></td>
							</tr>
						</table>
					</form>
					<%				
				} else {
			%>
					<h3>Welcome ${auname }${uname }</h3>
					<%				
			if(session.getAttribute("memberid")!=null){
				String mid = session.getAttribute("memberid").toString();
				List<MemberPics> pics = MemberPicsDAO.getallrecord(Integer.parseInt(mid));
				if(pics!=null && pics.size()>0){
					MemberPics pic = pics.get(0);
					String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
					out.println("<img width=\"70px\" height=\"70px\" class=\"magnify\" src=\""+path+"\"/><br/>");
				}else{
				String path = getServletContext().getRealPath("/upload/d.jpeg");
				out.println("<img width=\"70px\" height=\"70px\" class=\"magnify\" src=\""+path+"\"/><br/>");
				}
			}
			%>
					<%if(!Checks.isEmpty(uname)){ %>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="member/addmemberpic.jsp" />View/Upload Photos..</a>
					<%} %>
					<p>
						<%
				if (session.getAttribute("lastlogin") != null) {
						out.println("<br/><h3><span>Last Time You Visit : "
								+ session.getAttribute("lastlogin")
								+ "</span></h3>");
					} else {
						out.println("<h3><span>Welcome First Time in Our Site!!!</span></h3>");
					}

				}
			%>
					</p>
					<div>${message }</div>

				</div>
				<img src="images/img_11.jpg" alt="" width="631" height="279" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<%
					String message_forgotpassword = request.getAttribute("message_forgotpassword")!=null? request.getAttribute("message_forgotpassword").toString():"";
					if(!Checks.isEmpty(message_forgotpassword)){
					%>
					<h3>
						<span style="color: black;">${message_forgotpassword }</span>
					</h3>
					<br />
					<div class="bg"></div>
					<% }	%>
					<!-- main area where i write main code... -->
					<%
						String categoryid = request.getParameter("categoryid") != null? request.getParameter("categoryid") : "";
						CategoryInfo cinfo = CategoryInfoDAO.getSingleRecord(categoryid);
					%>
					<h2 align="center">
					<%if(cinfo != null){ %>
						<span><%=cinfo.getCategoryname() %></span>
					<%} %>
					</h2>
					<div class="bg"></div>
					<%						
					int pageno = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");
						List<ThreadInfo> allthread = ThreadInfoDAO.getallrecordOrderByThreadId(categoryid, pageno);
						int count = ThreadInfoDAO.getNumberOfrecord(categoryid);
						
						if(allthread != null){
						for(ThreadInfo record : allthread){
					%>
					<h3>						
						<!-- showing member pic....... -->		
					<%			
					List<MemberPics> pics = MemberPicsDAO.getallrecord(record.getMemberinfo().getMemberid());
							if(pics != null && pics.size() > 0){
								MemberPics pic = pics.get(0);
								String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
								out.println("<img width=\"50px\" height=\"50px\" src=\""+path+"\"/><br/>");
							}else{
								String path = getServletContext().getRealPath("/upload/d.jpg");
								out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
							}		
					%>													
					<!-- xxxxxxxxxxxxxxxxxxxshowing member pic....... -->
					<span><%=record.getMemberinfo().getMembername() %></span>
					</h3>
					<div class="clr"></div>														
					<h4>
						&nbsp;&nbsp;<%= record.getThreadtitle() %></h4>
					<% if(record.getThreadtext().length()>150){ %>
					<p><%=record.getThreadtext().substring(0, 150) %>...
					</p>
					<%}else{ %>
					<p><%=record.getThreadtext() %>
					</p>
					<%} %>
					<% out.println("<a href=\"viewsinglethread.jsp?thid="+record.getThreadid()+"\">");		%>
					<img src="images/reading.gif" alt="" width="118" height="26"
						border="0" /></a> <br /> &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<%=record.getThreaddate() %>
					<p>
						Total Visits:
						<%=record.getTotalvisitor() %></p>
					<div class="bg"></div>
					<%
							}
								//next page..	
							if(5*pageno >= count){
								
							}else{
								int i = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");
								out.println("<a href=\"viewthread_category.jsp?page="+ (i+1) +"&categoryid="+cinfo.getCategoryid()+"\" />Next Page</a>");
							}
						}else{
							out.println("<h2 align=\"center\">There is no further records coressponding to this category.</h2>");
						}
					%>
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				<!-- include sidebar according to user status whether its member / admin / not anyone of them -->
					<jsp:include page="DivClassRight.jsp"/>
				<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->				
		<div class="clr"></div>
		<!--  this is the tag to provide a clear line-->
	</div>
	</div>

	<jsp:include page="footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->

	<!-- Forgot password with popup.. -->
		<jsp:include page="forgotpassword.jsp"/>
	<!-- forgot pass form..end... --></body>
</html>