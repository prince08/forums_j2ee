
<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="java.util.List"%>
<div class="right">
	<div class="blog">
		<h2>
			<span>Sidebar</span> Menu
		</h2>
		<ul>			
			<li><a href="../home.jsp"><span>Home</span></a></li>
			<li><a href="addthreadinfo.jsp">Add Thread</a></li>
			<li><a href="viewthreadinfo.do">My Threads</a></li>
			<li><a href="../changepassword.jsp">Change Password</a></li>
			<li><a href="signout.jsp">Signout</a></li>							
		</ul>
	</div>

	<div class="blog">
		<h2>
			<span>Categories...</span>
		</h2>
		<ul class="category">
		<%
		
		  	List<CategoryInfo> allcategories = CategoryInfoDAO.getallrecord();
		  	if(allcategories != null){
		  		for(CategoryInfo d : allcategories){		 			
					out.println("<li class=\"category\">");
						out.println("<a href=\"../viewthread_category.jsp?categoryid="+ d.getCategoryid()+ "\">"+d.getCategoryname()+"</a><br />");
					out.println("</li>");
		  		}
		  	}
		
		%>
		</ul>
	</div>

	<div class="blog">
		<h2>Search</h2>
		<div class="search">
			<form id="form1" method="post" action="../searchresult.jsp">
				<span> <input name="searchtext" type="text" class="keywords"
					id="textfield" maxlength="50" placeholder="Search..." value="${param.searchtext }" />
				</span> <input name="b" type="image" src="images/search.gif" class="button" />
			</form>
		</div>
		<div class="clr"></div>
	</div>
</div>
