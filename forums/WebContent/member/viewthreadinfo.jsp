<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="model.dao.ThreadInfoDAO"%>
<%@page import="operations.Checks"%>
<%@page import="model.to.ThreadInfo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	
    response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	if (session.getAttribute("uname") == null) {
		response.sendRedirect("../home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/arial.js"></script>
<script type="text/javascript" src="../js/cuf_run.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({selector:'textarea',menubar:false});
</script>

</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<jsp:include page="../header.jsp" />
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->

					<h2 align="center">
						<span>My Threads</span>
					</h2>
					<div class="bg"></div>
					<%
	String message4 = request.getParameter("message4")!=null?request.getParameter("message4"):"";
	if(request.getAttribute("allrecords") != null){
		if(request.getAttribute("allrecords") instanceof List<?>){
			
			List<?> allrecords = (List<?>)request.getAttribute("allrecords");
			String c =request.getAttribute("threadid")!=null?request.getAttribute("threadid").toString():"";
			int count = ThreadInfoDAO.getNumberOfrecord(Integer.parseInt(session.getAttribute("memberid").toString()));
			int pageno = Integer.parseInt(request.getAttribute("pageno").toString());
			int threadid1 = 0;
			if(Checks.isEmpty(c)){
				//System.out.println("threadid is empty.");
			}else{
				threadid1 = Integer.parseInt(c);
			}
			for(Object d : allrecords)
			{
				if(d instanceof ThreadInfo)					
				{
					ThreadInfo data = (ThreadInfo) d;
					//System.out.println("hello");
					if(threadid1 == data.getThreadid()){
						out.println("<form method=\"post\" action = \"updatethreadinfo.do?threadid="+data.getThreadid()+"\">");
						out.println("<h3>Update Thread</h3>");
						out.println("<div class=\"bg\"></div>");
						out.println("<table cellspacing=\"5\" cellpadding=\"5\" align=\"center\">");
						out.println("<tr>");
						out.println("<td>Thread Title: </td><td><input type = \"text\" name = \"threadtitle\"  value = \""+data.getThreadtitle()+"\"/></td");
						out.println("</tr><tr>");						
						out.println("<td>Thread Text: </td><td><textarea name=\"threadtext\" rows=\"5\" cols=\"22\" style=\"resize:none\">"+data.getThreadtext()+"</textarea></td>");
						out.println("</tr><tr>");
						out.println("<td>Category:</td><td> <input type = \"text\" readonly=\"readonly\" name = \"categoryname\"  value = \""+data.getCategoryinfo().getCategoryname()+"\"/></td>");
						out.println("</tr><tr>");
						out.println("<td colspan=\"2\" align=\"center\"><input type=\"submit\" value = \"Update\"/></td>");
						out.println("</tr>");
						out.println("</table>");
						out.println("</form>");
						out.println("<div class=\"bg\"></div>");
					}else{	
				%>
					<h3>						
						<!-- showing member pic....... -->		
					<%			
					List<MemberPics> pics = MemberPicsDAO.getallrecord(data.getMemberinfo().getMemberid());
							if(pics != null && pics.size() > 0){
								MemberPics pic = pics.get(0);
								String path = "../upload/" + pic.getPicid() + "." + pic.getExtname();
								out.println("<img width=\"50px\" height=\"50px\" src=\""+path+"\"/><br/>");
							}else{
								String path = getServletContext().getRealPath("../upload/d.jpg");
								out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
							}		
					%>													
					<!-- xxxxxxxxxxxxxxxxxxxshowing member pic....... -->
					<span><%=data.getMemberinfo().getMembername() %></span>
					</h3>
					
					<div class="clr"></div>									
					 <h4>&nbsp;&nbsp;<%= data.getThreadtitle() %></h4>
					 <% if(data.getThreadtext().length()>150){ %>
					<p><%=data.getThreadtext().substring(0, 150) %>...</p>
					<%} %>					
					<% out.println("<a href=\"../viewsinglethread.jsp?thid="+data.getThreadid()+"\">");%>
					<img src="images/reading.gif" alt="" width="118" height="26"
						border="0" /></a> <br /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<%=data.getThreaddate() %>
					<p>Total Visits: <%=data.getTotalvisitor() %></p>
					<br />
					<%						
						out.println("<a href=\"viewthreadinfo.do?threadid1="+ data.getThreadid()+ "\">");
						out.println("<img src=\"images/edit_logo.png\" height=\"25px\" width=\"25px\" /></a>");
						
						out.println("<a onclick=\"return confirm('Are you sure to delete this record?')\" href=\"deletethreadinfo.do?threadid2="+ data.getThreadid()+ "\">");
						out.println("<img src=\"images/del_logo.png\" height=\"25px\" width=\"25px\" /></a>");						
					%>					
					<div class="bg"></div>
					<%											
					}				
				}
			}	
				//next page..
			if(5*pageno >= count){
				
			}else{
				int i = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");
				out.println("<a href=\"viewthreadinfo.do?page="+ (i+1) +"\" />Next Page</a>");
			}
		}
	}
	else{
		out.println("<h2 align=\"center\">There is No Data</h2>");
	}
%>

					<h2>${message_viewthreadinfo }</h2>
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="../footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>