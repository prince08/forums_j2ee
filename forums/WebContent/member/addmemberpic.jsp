<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="java.util.List"%>
<%@page import="operations.Checks"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	if (session.getAttribute("uname") == null) {
		response.sendRedirect("../home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/arial.js"></script>
<script type="text/javascript" src="../js/cuf_run.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/jquerymagnifier.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#fileForm").validate();
	});
</script>
</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<jsp:include page="../header.jsp" />
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<form method="post" action="insertmemberpics.do"
						enctype="multipart/form-data" class="cmxform" id="fileForm">
						<table cellpadding="5" cellspacing="5">
							<tr>
								<td>Choose Picture:</td>
								<td><input type="file" id="file" name="file" class="required" accept="image/*" /></td>
							</tr>
							<tr>
								<td colspan="2"><input align="middle" type="submit"
									value="Upload" /></td>
							</tr>
						</table>
					</form>

					<table cellpadding="10" cellspacing="10">
						<%
						String memberid = "";
						String message_showpic="";
						int i=0;
						if(session.getAttribute("memberid")!=null){
							memberid = session.getAttribute("memberid").toString();
						List<MemberPics> allpics = MemberPicsDAO.getallrecord(Integer.parseInt(memberid));
							if(allpics != null && allpics.size()>0){
								for(MemberPics pic : allpics){
									String path = "../upload/" + pic.getPicid() + "." + pic.getExtname();
									String picname = pic.getPicid() + "." + pic.getExtname();
									if(i==0){
										out.println("<tr>");
									}
									out.println("<td>");
									out.println("<img width=\"150px\" height=\"150px\" class=\"magnify\" src=\""+path+"\"/><br/>");
									out.println("<a href=\"downloadpic.do?picname=" + picname + "\"><img src=\"images/download_logo.png\" height=\"25px\" width=\"25px\" /></a>");									
									out.println("<a onclick=\"return confirm('Are you sure to delete this Photo?')\" href=\"deletememberpics.do?picid=" + pic.getPicid() + "\">");
									out.println("<img src=\"images/del_logo.png\" height=\"25px\" width=\"25px\" /></a>");
									out.println("</td>");
									i++;
									if(i==3){
										out.println("</tr>");				
										i=0;
									}	
								}
								if(i!=0)
								{
									out.println("</tr>");
								}
							}else{
								message_showpic = "<h1>There is no Photos.</h1>";
							}
						}						
					%>
					</table>
					<%=message_showpic %>

					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="../footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>