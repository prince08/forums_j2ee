<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="model.dao.ThreadInfoDAO"%>
<%@page import="model.to.ThreadInfo"%>
<%@page import="java.util.List"%>
<%@page import="model.to.MemberInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	if (session.getAttribute("uname") == null) {
		response.sendRedirect("../home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/arial.js"></script>
<script type="text/javascript" src="../js/cuf_run.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/custom.js" type="text/javascript"></script>
</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<jsp:include page="../header.jsp" />
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->

					<h1>Welcome Member</h1>
					<div class="bg"></div>
					<%					
						int pageno = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");						
						List<ThreadInfo> allthreads = ThreadInfoDAO.getallrecordOrderByThreadId(pageno);
						int count = ThreadInfoDAO.getNumberOfRecord();
						if(allthreads != null){
						for(ThreadInfo record : allthreads){
					%>
					<h3>						
						<!-- showing member pic....... -->		
					<%			
					List<MemberPics> pics = MemberPicsDAO.getallrecord(record.getMemberinfo().getMemberid());
							if(pics != null && pics.size() > 0){
								MemberPics pic = pics.get(0);
								String path = "../upload/" + pic.getPicid() + "." + pic.getExtname();
								out.println("<img width=\"50px\" height=\"50px\" src=\""+path+"\"/><br/>");
							}else{
								String path = getServletContext().getRealPath("../upload/d.jpg");
								out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
							}		
					%>													
					<!-- xxxxxxxxxxxxxxxxxxxshowing member pic....... -->
					<span><%=record.getMemberinfo().getMembername() %></span>
					</h3>
					<div class="clr"></div>																			
					<h4>
						&nbsp;&nbsp;<%= record.getThreadtitle() %></h4>
					<% if(record.getThreadtext().length()>150){ %>
					<p><%=record.getThreadtext().substring(0, 150) %>...
					</p>
					<%}else{ %>
					<p><%=record.getThreadtext() %>
					</p>
					<%} %>
					<% out.println("<a href=\"../viewsinglethread.jsp?thid="+record.getThreadid()+"\">");%>
					<img src="images/reading.gif" alt="" width="118" height="26"
						border="0" /></a> <br /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<%=record.getThreaddate() %>
					<p>
						Total Visits:
						<%=record.getTotalvisitor() %></p>
					<div class="bg"></div>
					<%
							}
						//next page..						
						if(5*pageno >= count){
							
						}else{
							int i = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");
							out.println("<a href=\"index.jsp?page="+ (i+1) +"\" />Next Page</a>");
						}		
					}						
					%>
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="../footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>