<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="java.util.List"%>
<%@page import="model.to.MemberInfo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	if (session.getAttribute("uname") == null) {
		response.sendRedirect("../home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="../style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/cufon-yui.js"></script>
<script type="text/javascript" src="../js/arial.js"></script>
<script type="text/javascript" src="../js/cuf_run.js"></script>
<script src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector : 'textarea',
		menubar : false
	});
</script>
</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<jsp:include page="../header.jsp" />
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<%						
		String threadtitle = request.getParameter("threadtitle") != null ? request
				.getParameter("threadtitle").trim() : "";
		String threadtext = request.getParameter("threadtext") != null ? request
				.getParameter("threadtext") : "";		
%>
					<form method="post" action="insertthreadinfo.do">
						<h2 align="center">
							<span>....Thread Info.....</span>
						</h2>
						<div class="bg"></div>
						<table cellpadding="10" cellspacing="10" align="center">
							<tr>
								<td>Thread Title:</td>
								<td><input type="text" name="threadtitle"
									value=${param.threadtitle }></input></td>
							</tr>

							<tr>
								<td>Thread Text:</td>
								<td><textarea name="threadtext" rows="5" cols="22"
										style="resize: none">${param.threadtext } </textarea></td>
							</tr>
							<tr>
								<td>Select a category by its id:</td>
								<td><select name="categoryid">
										<%
						//out.println("hello");
						List<CategoryInfo> allcategoryinfo = CategoryInfoDAO.getallrecord();							
					 	for(CategoryInfo value: allcategoryinfo)
						{
							if("${param.categoryid }".equals(value.getCategoryid())){
								out.println("<option selected = \"selected\" value=\""+value.getCategoryid()+"\">"+value.getCategoryid()+": "+value.getCategoryname()+"</option> ");
							}else
								out.println("<option value=\""+value.getCategoryid()+"\">"+value.getCategoryid()+": "+value.getCategoryname()+"</option> ");							
						}
					%>
								</select></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Submit.." name="submit" /></td>
							</tr>
						</table>
					</form>

					<h4 align="center">${message_addthreadinfo }</h4>

					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="../footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>