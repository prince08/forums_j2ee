<%@page import="operations.QuestionsList"%>
<%@page import="java.util.ArrayList"%>
<div id="myModal" class="reveal-modal">

		<h3 align="center">
			<span>Forgot password...</span>
		</h3>
		<form method="post" action="forgotpassword.do" id="ForgotPassForm">
			<table cellpadding="5" cellspacing="5" align="center">
				<tr>
					<td>Enter user name :</td>
					<td><input type="text" name="username"
						value="${param.username }" /></td>
				</tr>
				<tr>
					<td>Choose Security question :</td>
					<td><select name="sq">
							<%
						ArrayList<String> questions  = QuestionsList.getQuestions();						
						for(String value: questions)
						{
							if("${param.sq }".equals(value)){
								out.println("<option selected = \"selected\" value=\""+value+"\">"+value+"</option> ");
							}else
								out.println("<option value=\""+value+"\">"+value+"</option> ");							
						}
					%>
					</select></td>
				</tr>
				<tr>
					<td>Enter Security answer: :</td>
					<td><input type="text" name="sans" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><input type="submit"
						name="reset" value="Reset Password" /></td>
				</tr>
			</table>
		</form>
		<!-- ${message_forgotpassword }  -->
		<a class="close-reveal-modal">&#215;</a>
	</div>
