<%@page import="model.to.ReplyRatingInfo"%>
<%@page import="model.dao.ReplyRatingInfoDAO"%>
<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="model.dao.MemberInfoDAO"%>
<%@page import="model.dao.ThreadReplyInfoDAO"%>
<%@page import="model.to.ThreadReplyInfo"%>
<%@page import="model.dao.ThreadInfoDAO"%>
<%@page import="model.to.ThreadInfo"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="java.util.List"%>
<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="operations.QuestionsList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="operations.Checks"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1	

	String uname = session.getAttribute("uname") != null ? session
			.getAttribute("uname").toString() : "";
	String auname = session.getAttribute("auname") != null ? session
			.getAttribute("auname").toString() : "";		
	if(request.getParameter("thid") == null && request.getAttribute("thid1") == null) {
		response.sendRedirect("home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" type="text/css" href="reveal.css" />
<script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#ForgotPassForm").validate({
			rules : {
				username : {
					required : true,
				},
				sq : "required",
				sans : {
					required : true,
				}
			},
			messages : {
				username : {
					required : " Required",
				},
				sq : " Please select a Security question",
				sans : {
					required : " Required.",
				}
			}
		});
	});
</script>
<script type="text/javascript">
	tinymce.init({
		selector : 'textarea',
		menubar : false
	});
</script>

</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<!-- header...................................xxxxxxxxxxxxxxxxxxxxxxxxxxx -->
		<div class="header">
			<div class="header_resize">
				<div class="logo">
					<h1>
						<a href="home.jsp">Forum<small>Where we answer to
								every problem...</small></a>
					</h1>
				</div>
				<div class="menu">
					<ul>

						<li><a href="home.jsp" class="active"><span>Home</span></a></li>
						<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
						<li><a href="contact.jsp"><span> Contact Us</span></a></li>
					</ul>
				</div>
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
			<div class="headert_text_resize">
				<div class="textarea">

					<%
				if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
			%>
					<h2>Login</h2>
					<form method="post" action="checklogin.do">
						<table>
							<tr>
								<td>UserName:</td>
								<td><input type="text" name="username"
									value="${param.username }" placeholder="Username" /></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password"
									placeholder="Password" /></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><input type="submit"
									name="login" value="LOGIN" /></td>

							</tr>
							<tr>
								<td align="center" colspan="2"><a href="#"
									data-reveal-id="myModal" data-animation="fade">Forgot
										password...</a></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><a href="addLoginInfo.jsp">Not
										having an account...Click here!!...</a></td>
							</tr>
						</table>
					</form>
					<%
				} else {
			%>
					<h3>Welcome ${auname }${uname }</h3>
					<%				
			if(session.getAttribute("memberid")!=null){
				String mid = session.getAttribute("memberid").toString();
				List<MemberPics> pics = MemberPicsDAO.getallrecord(Integer.parseInt(mid));
				if(pics!=null && pics.size()>0){
					MemberPics pic = pics.get(0);
					String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
					out.println("<img width=\"70px\" height=\"70px\" class=\"magnify\" src=\""+path+"\"/><br/>");
				}else{
				String path = getServletContext().getRealPath("/upload/d.jpeg");
				out.println("<img width=\"70px\" height=\"70px\" class=\"magnify\" src=\""+path+"\"/><br/>");
				}
			}
			%>

					<%if(!Checks.isEmpty(uname)){ %>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="member/addmemberpic.jsp" />View/Upload Photos..</a>
					<%} %>
					<p>
						<%
				if (session.getAttribute("lastlogin") != null) {
						out.println("<br/><h3><span>Last Time You Visit : "
								+ session.getAttribute("lastlogin")
								+ "</span></h3>");
					} else {
						out.println("<h3><span>Welcome First Time in Our Site!!!</span></h3>");
					}

				}
			%>
					</p>
					<div>${message }</div>

				</div>
				<img src="images/img_11.jpg" alt="" width="631" height="279" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>
		<!-- header end....xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<%
					if(request.getParameter("thid")!=null || request.getAttribute("thid1")!=null){
						String thid = request.getParameter("thid") != null ? request.getParameter("thid").toString().trim() : request.getAttribute("thid1").toString().trim() ;
						int threadid = Integer.parseInt(thid);
						ThreadInfo data = ThreadInfoDAO.getSinglerecord(threadid);
						if(data!=null){
							data.setTotalvisitor(data.getTotalvisitor()+1);
							
					%>
					<h3>
						<!-- showing member pic....... -->
						<%			
					List<MemberPics> pics1 = MemberPicsDAO.getallrecord(data.getMemberinfo().getMemberid());
							if(pics1 != null && pics1.size() > 0){
								MemberPics pic = pics1.get(0);
								String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
								out.println("<img width=\"50px\" height=\"50px\" src=\""+path+"\"/><br/>");
							}else{
								String path = getServletContext().getRealPath("/upload/d.jpg");
								out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
							}		
					%>
						<!-- xxxxxxxxxxxxxxxxxxx  showing member pic....... -->
						<span><%=data.getMemberinfo().getMembername() %></span>
					</h3>
					<div class="clr"></div>
					<h4>
						&nbsp;&nbsp;<%= data.getThreadtitle() %>
					</h4>
					<!--  this is the thread info.......############################ -->
						<%=data.getThreadtext() %>
					
					<br />
					<p>
						Total Visits:
						<%=data.getTotalvisitor() %>
					</p>
					<%
						if(session.getAttribute("memberid")!=null){
							if(data.getMemberinfo().getMemberid() == Integer.parseInt(session.getAttribute("memberid").toString())){
								out.println("<a onclick=\"return confirm('Are you sure to delete this Record?')\" href=\"member/deletethreadinfo.do?threadid2="+ data.getThreadid()+ "\">");
								out.println("<img src=\"images/del_logo.png\" height=\"25px\" width=\"25px\" /><br/>Delete Thread</a>");
							}
						}
					%>
					<div class="bg"></div>
					<h4>
						<span style="color: orange;"><b>Comments...</b></span>
					</h4>
					<div class="bg"></div>
					<!-- xxxxxxx Comments...xxxxxx -->
					<%
							List<ThreadReplyInfo> allcomments = ThreadReplyInfoDAO.getallrecordOrderByReplyId(data.getThreadid());
							if(allcomments!=null){
								for(ThreadReplyInfo d:allcomments){
									//Member Pic
									List<MemberPics> pics = MemberPicsDAO.getallrecord(d.getMemberinfo().getMemberid());
									if(pics != null && pics.size() > 0){
										MemberPics pic = pics.get(0);
										String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
										out.println("<img width=\"50px\" height=\"50px\" src=\""+path+"\"/><br/>");
									}else{
										String path = getServletContext().getRealPath("/upload/d.jpg");
										out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
									}
									//Member Name
									out.println("&nbsp; &nbsp;<b>"+d.getMemberinfo().getMembername()+"</b>");									
									
									//comment									
									out.println("<p>"+d.getReplytext()+"</p>");
																		
									//delete reply.....(comments.)
									if(session.getAttribute("memberid")!=null){
										if(Integer.parseInt(session.getAttribute("memberid").toString()) == d.getMemberinfo().getMemberid()){
											out.println("<a onclick=\"return confirm('Are you sure to delete this Record?')\" href=\"deletethreadreplyinfo.do?replyid="+ d.getReplyid()+ "&thid1="+d.getThreadinfo().getThreadid()+"\">");
											out.println("<img src=\"images/del_logo.png\" height=\"25px\" width=\"25px\" /></a>");											
										}
									}																																
						// Rating of the comment.												
									out.println("&nbsp;&nbsp;&nbsp;&nbsp;");
									out.println("<b>Rating: "+ReplyRatingInfoDAO.getAverage(d.getReplyid())+"</b>");
						// rating the reply......xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
									if (session.getAttribute("uname") != null) {
				//out.println("<h1>"+ReplyRatingInfoDAO.isRecord(d.getReplyid(), Integer.parseInt(session.getAttribute("memberid").toString()))+"</h1>");
											List<ReplyRatingInfo> temp = ReplyRatingInfoDAO.isRecord(d.getReplyid(), Integer.parseInt(session.getAttribute("memberid").toString()));
									if(temp == null){										
											out.println("<form method=\"get\" action=\"insertreplyratinginfo.do\">");
												out.println("&nbsp;&nbsp;&nbsp;&nbsp; Rate the Comment: ");
												out.println("<input type=\"hidden\" name=\"replyid\" value=\""+d.getReplyid()+"\"/>");
												out.println("<input type=\"hidden\" name=\"thid1\" value=\""+d.getThreadinfo().getThreadid()+"\"/>");
												out.println("<select name=\"rating\">");
												for (int i = 1; i <= 10; i++) {
													out.println("<option value=\"" + i + "\">" + i+ "</option> ");
												}
												out.println("</select>");		
												out.println("<input type=\"submit\" value=\"Rate..\"/>");
											out.println("</form>");
				//out.println(request.getAttribute("message_rating")!=null?request.getAttribute("message_rating"):"");
									}	
								}																													
							//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
									out.println("<div class=\"bg\"></div>");
								}
							}
						%>
					<!-- End of comments.. -->
					<!-- reply box... -->
					<%if(Checks.isEmpty(uname) && Checks.isEmpty(auname)){ %>
					<!--  user is not a member or admin so can't comment.....	 -->
					<h4>You have to register to comment here...</h4>
					<%}else if(!Checks.isEmpty(uname)){ %>
					<p>Write a comment...</p>
					<form action="insertthreadreplyinfo.do" method="get">
						<table cellpadding="5" cellspacing="5">
							<tr>
								<td><input type="hidden" value="<%=data.getThreadid() %>"
									name="thid" /> <textarea name="replytext" rows="5" cols="25"
										style="resize: none"></textarea></td>
								<td><input type="submit" value="Post" /></td>
							</tr>
						</table>
					</form>
					<% }}}else{ %>
					<h2>There is an internal error...</h2>
					<% } %>
					<!-- <h4>${message_threadreply }</h4>   -->
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				<!-- include sidebar according to user status whether its member / admin / not anyone of them -->
				<jsp:include page="DivClassRight.jsp" />
				<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->

	<!-- Forgot password with popup.. -->
	<jsp:include page="forgotpassword.jsp" />
	<!-- forgot pass form..end... -->
</body>
</html>