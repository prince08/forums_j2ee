<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="operations.Checks"%>
<%@page import="model.to.CategoryInfo"%>
<%@page import="model.dao.CategoryInfoDAO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String uname = session.getAttribute("uname") != null ? session
			.getAttribute("uname").toString() : "";
	String auname = session.getAttribute("auname") != null ? session
			.getAttribute("auname").toString() : "";
	if (session.getAttribute("auname") == null
			&& session.getAttribute("uname") == null) {
		response.sendRedirect("home.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" type="text/css" href="reveal.css" />
</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
		<!-- header,... -->
		<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
		<div class="header">
			<div class="header_resize">
				<div class="logo">
					<h1>
						<a href="home.jsp">Forum<small>Where we answer to
								every problem...</small></a>
					</h1>
				</div>
				<div class="menu">
					<ul>
						<li><a href="home.jsp" class="active"><span>Home</span></a></li>
						<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
						<li><a href="contact.jsp"><span> Contact Us</span></a></li>
					</ul>
				</div>
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
			<div class="headert_text_resize">
				<div class="textarea">
					<%
						if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
							// No user is logged in......
						} else {
					%>
					<h3>Welcome ${auname }${uname }</h3>

					<%				
			if(session.getAttribute("memberid")!=null){
				String mid = session.getAttribute("memberid").toString();
				List<MemberPics> pics = MemberPicsDAO.getallrecord(Integer.parseInt(mid));
				if(pics!=null && pics.size()>0){
					MemberPics pic = pics.get(0);
					String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
					out.println("<img width=\"70px\" height=\"70px\" class=\"magnify\" src=\""+path+"\"/><br/>");
				}else{
				String path = getServletContext().getRealPath("/upload/d.jpeg");
				out.println("<img width=\"70px\" height=\"70px\" class=\"magnify\" src=\""+path+"\"/><br/>");
				}
			}
			%>

					<%if(!Checks.isEmpty(uname)){ %>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a
						href="member/addmemberpic.jsp" />View/Upload Photos..</a>
					<%} %>


					<p>
						<%
				if (session.getAttribute("lastlogin") != null) {
						out.println("<br/><h3><span>Last Time You Visit : "
								+ session.getAttribute("lastlogin")
								+ "</span></h3>");
					} else {
						out.println("<h3><span>Welcome First Time in Our Site!!!</span></h3>");
					}

				}
			%>
					</p>
				</div>
				<img src="images/img_11.jpg" alt="" width="631" height="279" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>
		<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
		<div class="body">
			<div class="body_resize">
				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<h2 align="center">
						<span>Change Password</span>
					</h2>
					<div class="bg"></div>
					<form action="changepassword.do" method="post">
						<table cellpadding="5" cellspacing="5" align="center">
							<tr>
								<td>Old Password:</td>
								<td><input type="password" name="oldpassword"
									placeholder="Old Password" /></td>
							</tr>
							<tr>
								<td>New Password:</td>
								<td><input type="password" name="newpassword"
									placeholder="New Password" /></td>
							</tr>
							<tr>
								<td>Confirm New Password:</td>
								<td><input type="password" name="newpassword1"
									placeholder="Confirm New Password" /></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="proceed" /></td>
							</tr>
						</table>
					</form>
					<h2 align="center">${message_changepassword }</h2>

					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>

				<!-- div class right... -->
				<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<jsp:include page="DivClassRight.jsp" />
				<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>