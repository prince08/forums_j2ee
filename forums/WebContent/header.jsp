
<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="java.util.List"%>
<%@page import="operations.Checks"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1	

	String uname = session.getAttribute("uname") != null ? session
			.getAttribute("uname").toString() : "";
	String auname = session.getAttribute("auname") != null ? session
			.getAttribute("auname").toString() : "";
%>

<div class="header">
	<div class="header_resize">
		<div class="logo">
			<h1>
				<a href="#">Forum<small>Where we answer to every
						problem...</small></a>
			</h1>
		</div>
		<div class="menu">
			<ul>
				<%
					if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
				%>
					<li><a href="home.jsp" class="active"><span>Home</span></a></li>
					<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
					<li><a href="contact.jsp"><span> Contact Us</span></a></li>
				<%
					} else {
				%>
					<li><a href="../home.jsp" class="active"><span>Home</span></a></li>
					<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
					<li><a href="../contact.jsp"><span> Contact Us</span></a></li>
				<%
					}
				%>				
			</ul>
		</div>
		<div class="clr"></div>
		<!--  this is the tag to provide a clear line-->
	</div>
	<div class="headert_text_resize">
		<div class="textarea">

			<%
				if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
			%>
			<h2>Login</h2>
			<form method="post" action="checklogin.do">
				<table>
					<tr>
						<td>UserName:</td>
						<td><input type="text" name="username"
							value="${param.username }" placeholder="Username" /></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password"
							placeholder="Password" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><input type="submit"
							name="login" value="LOGIN" /></td>

					</tr>
					<tr>
						<td align="center" colspan="2"><a href="#"
									data-reveal-id="myModal" data-animation="fade">Forgot
										password...</a></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><a href="addLoginInfo.jsp">Not
								having an account...Click here!!...</a></td>
					</tr>
				</table>
			</form>
			<%
				} else {
			%>
			<h3>Welcome ${auname }${uname }</h3>
			<%
				
			if(session.getAttribute("memberid")!=null){
				String mid = session.getAttribute("memberid").toString();
				List<MemberPics> pics = MemberPicsDAO.getallrecord(Integer.parseInt(mid));
				if(pics!=null && pics.size()>0){
					MemberPics pic = pics.get(0);
					String path = "../upload/" + pic.getPicid() + "." + pic.getExtname();
					out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
				}else{
				String path = getServletContext().getRealPath("/upload/d.jpeg");
				out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
				}
			}
			%>

			<%if(!Checks.isEmpty(uname)){ %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="addmemberpic.jsp" />View/Upload Photos..</a>
			<%} %>
			<p>
				<%
				if (session.getAttribute("lastlogin") != null) {
						out.println("<br/><h3><span>Last Time You Visit : "
								+ session.getAttribute("lastlogin")
								+ "</span></h3>");
					} else {
						out.println("<h3><span>Welcome First Time in Our Site!!!</span></h3>");
					}

				}
			%>
			</p>
			<div>${message }</div>

		</div>
		<img src="images/img_11.jpg" alt="" width="631" height="279" />
		<div class="clr"></div>
		<!--  this is the tag to provide a clear line-->
	</div>
</div>