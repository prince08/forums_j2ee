<%@page import="operations.QuestionsList"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1	
	String uname = session.getAttribute("uname") != null ? session
			.getAttribute("uname").toString() : "";
	String auname = session.getAttribute("auname") != null ? session
			.getAttribute("auname").toString() : "";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script>
	$(document).ready(
					function() {
						$("#registration").validate(
										{
											rules : {
												membername : {
													required : true,
													minlength : 1
												},
												username : {
													required : true,
													minlength : 5
												},
												password1 : {
													required : true,
													minlength : 5
												},
												password2 : {
													required : true,
													minlength : 5,
													equalTo : "#password"
												},
												rolename : "required",
												sq : "required",
												sans : {
													required : true,
													minlength : 4
												},
												phoneno : {
													required : true,
													number : true													
												},
												emailid : {
													required : true,
													email : true
												}
											},
											messages : {
												membername : {
													required : " This field is required.",
													minlength : " Please enter Atleast 1 letter Name"
												},
												username : {
													required : " This field is required.",
													minlength : " Please enter Atleast 5 letter Name"
												},
												password1 : {
													required : " Please provide a password",
													minlength : " Your password must be at least 5 characters long"
												},
												password2 : {
													required : " Please provide a password",
													minlength : " Your password must be at least 5 characters long",
													equalTo : " Please enter the same password as above"
												},
												rolename : "This field is required.",
												sq : " Please select a Security question",
												sans : {
													required : " Please provide a Security Ansswer.",
													minlength : " Your password must be at least 5 characters long",
												},
												phoneno : {
													required : " This field is required.",
													number : " Please enter the valid Phone number"													
												},
												emailid : {
													required : " This field is required.",
													email : " Please enter a valid email-ID."
												}
											}
										});
					});
</script>

</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">

		<!-- ---header.... -->
		<div class="header">
			<div class="header_resize">
				<div class="logo">
					<h1>
						<a href="home.jsp">Forum<small>Where we answer to
								every problem...</small></a>
					</h1>
				</div>
				<div class="menu">
					<ul>
						<li><a href="home.jsp" class="active"><span>Home</span></a></li>
						<!-- 						<li><a href="#"><span> About Us </span></a></li>	 -->
						<li><a href="contact.jsp"><span> Contact Us</span></a></li>
					</ul>
				</div>
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
			<div class="headert_text_resize">
				<div class="textarea">
					<%					
				if (uname.equalsIgnoreCase("") && auname.equalsIgnoreCase("")) {
			%>
					<h2 align="center">Registration</h2>


					<%
				} else {
					response.sendRedirect("home.jsp");
				}
			%>
				</div>
				<img src="images/img_11.jpg" alt="" width="631" height="279" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>
		<!-- -----end header.... -->

		<div class="body">
			<div class="body_resize">
				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<!-- main area where i write main code... -->
					<%
	String username = request.getParameter("username") != null ? request
		.getParameter("username").trim() : "";
	String password1 = request.getParameter("password1") != null ? request
		.getParameter("password1").trim() : "";
	String password2 = request.getParameter("password2") != null ? request
				.getParameter("password2").trim() : "";			
	String rolename = request.getParameter("rolename") != null ? request
		.getParameter("rolename") : "";
		String[] values = { "Member"};	
	String sq = request.getParameter("sq") != null ? request.getParameter(
		"sq").trim() : "";
	String sans = request.getParameter("sans") != null ? request
		.getParameter("sans").trim() : "";
	String phoneno = request.getParameter("phoneno") != null ? request
		.getParameter("phoneno").trim() : "";
	String emailid = request.getParameter("emailid") != null ? request
				.getParameter("emailid").trim() : "";
	String membername = request.getParameter("membername") != null ? request
						.getParameter("membername").trim() : "";				
%>
					<form method="post" action="insertlogininfo.do" id="registration">
						<h1 align="center">
							<span>Registration Form</span>
							<div class="bg"></div>
						</h1>
						<table cellpadding="5" cellspacing="10" align="center">
							<tr>
								<td>Name:</td>
								<td><input type="text" name="membername"
									value=<%=membername %>></input></td>
							</tr>
							<tr>
								<td>UserName:</td>
								<td><input type="text" name="username" value=<%=username %>></input></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password1" id="password"></input></td>
							</tr>
							<tr>
								<td>Confirm Password:</td>
								<td><input type="password" name="password2"></input></td>
							</tr>
							<tr>
								<td>RoleName:</td>
								<td>
									<%					
					for (String value : values) 
					{
						String checkstatus = "";
						if (value.equals(rolename)) 
						{
						checkstatus = "checked=\"checked\"";
						}						
						out.println(value + " <input type=\"radio\" name=\"rolename\" "+ checkstatus + "value=\"" + value + "\"/>");
					}
			%>
								</td>
							</tr>
							<tr>
								<td>Security question:</td>
								<td><select name="sq">
										<%
						//out.println("hello");
						ArrayList<String> questions  = QuestionsList.getQuestions();						
					 	for(String value: questions)
						{
							if("${param.sq }".equals(value)){
								out.println("<option selected = \"selected\" value=\""+value+"\">"+value+"</option> ");
							}else
								out.println("<option value=\""+value+"\">"+value+"</option> ");							
						}
					%>
								</select></td>
							</tr>
							<tr>
								<td>Answer:</td>
								<td><input type="text" name="sans" value=<%=sans %>></input></td>
							</tr>
							<tr>
								<td>Phone Number:</td>
								<td><input type="text" name="phoneno" maxlength="10" value=<%=phoneno %>></input></td>
							</tr>

							<tr>
								<td>Email-Id:</td>
								<td><input type="text" name="emailid" value=<%=emailid %>></input></td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit"
									value="Register" name="submit" /></td>
							</tr>
						</table>
					</form>
					<!-- 	<h2 align="center"><span>${message_addlogininfo }</span></h2>	 -->
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>
		<jsp:include page="footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->
</body>
</html>