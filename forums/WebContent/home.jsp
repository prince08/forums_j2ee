<%@page import="model.dao.MemberPicsDAO"%>
<%@page import="model.to.MemberPics"%>
<%@page import="model.dao.ThreadInfoDAO"%>
<%@page import="model.to.ThreadInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="operations.QuestionsList"%>
<%@page import="operations.Checks"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	response.setHeader("Cache-Control", "no-cache");//HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0); //prevents caching 
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1	

	if (session.getAttribute("uname") != null) {
		response.sendRedirect("member/index.jsp");
	} else if (session.getAttribute("auname") != null) {
		response.sendRedirect("admin/index.jsp");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Forum</title>
<link rel="shortcut icon" href="favicon.ico" />

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/arial.js"></script>
<script type="text/javascript" src="js/cuf_run.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#ForgotPassForm").validate({
			rules : {
				username : {
					required : true,
				},
				sq : "required",
				sans : {
					required : true,
				}
			},
			messages : {
				username : {
					required : " Required.",
				},
				sq : " Please select a Security question",
				sans : {
					required : " Required.",
				}
			}
		});
	});
</script>

<link rel="stylesheet" type="text/css" href="reveal.css" />
</head>
<body>
	<!-- START PAGE SOURCE -->
	<div class="main">
	
			<jsp:include page="header.jsp" />
			
		<div class="body">
			<div class="body_resize">

				<div class="left">
					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
					<%
					String message_forgotpassword = request.getAttribute("message_forgotpassword")!=null? request.getAttribute("message_forgotpassword").toString():"";
					if(!Checks.isEmpty(message_forgotpassword)){
					%>
					<h3>
						<span style="color: black;">${message_forgotpassword }</span>
					</h3>
					<br />
					<div class="bg"></div>
					<% }	%>
				
		<!-- main area where i write main code... -->
					<%
						int pageno = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");
						List<ThreadInfo> allthreads = ThreadInfoDAO.getallrecordOrderByThreadId(pageno);
						int count = ThreadInfoDAO.getNumberOfRecord();
						//out.println("<h1>number of record: "+count+"</h1>");
						if(allthreads != null){
						for(ThreadInfo record : allthreads){
					%>
					
					<h3>						
						<!-- showing member pic....... -->		
					<%			
					List<MemberPics> pics = MemberPicsDAO.getallrecord(record.getMemberinfo().getMemberid());
							if(pics != null && pics.size() > 0){
								MemberPics pic = pics.get(0);
								String path = "upload/" + pic.getPicid() + "." + pic.getExtname();
								out.println("<img width=\"50px\" height=\"50px\" src=\""+path+"\"/><br/>");
							}else{
								String path = getServletContext().getRealPath("/upload/d.jpg");
								out.println("<img width=\"70px\" height=\"70px\" src=\""+path+"\"/><br/>");
							}		
					%>													
					<!-- xxxxxxxxxxxxxxxxxxx  showing member pic....... -->
					<span>&nbsp;<%=record.getMemberinfo().getMembername() %></span>
					</h3>					
					<div class="clr"></div>									
					<h4>  
						&nbsp;&nbsp;<%= record.getThreadtitle() %>
					</h4>
					<% if(record.getThreadtext().length()>150){ %>
					<p><%=record.getThreadtext().substring(0, 150) %>...
					</p>
					<%}else{ %>
					<p><%=record.getThreadtext() %>
					</p>
					<%} %>
					<% out.println("<a href=\"viewsinglethread.jsp?thid="+record.getThreadid()+"\">");%>
					<img src="images/reading.gif" alt="" width="118" height="26"
						border="0" /></a> <br /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<%=record.getThreaddate() %>
					<p>
						Total Visits:
						<%=record.getTotalvisitor() %></p>
					<div class="bg"></div>
					<%
							}
					//next page..	
						if(5*pageno >= count){
							
						}else{
							int i = Integer.parseInt(request.getParameter("page")!=null ? request.getParameter("page") : "1");
							out.println("<a href=\"home.jsp?page="+ (i+1) +"\" />Next Page</a>");
						}		
					}
					%>

					<!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
				</div>
				<jsp:include page="DivClassRight.jsp" />
				<div class="clr"></div>
				<!--  this is the tag to provide a clear line-->
			</div>
		</div>

		<jsp:include page="footer.jsp" />
	</div>
	<!-- END PAGE SOURCE -->

	<!-- Forgot password with popup.. -->
	<jsp:include page="forgotpassword.jsp" />
	<!-- forgot pass form..end... -->
</body>
</html>