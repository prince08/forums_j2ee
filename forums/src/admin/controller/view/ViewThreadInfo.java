package admin.controller.view;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.ThreadInfoDAO;
import model.to.ThreadInfo;

@WebServlet("/member/viewthreadinfo.do")
public class ViewThreadInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String memberid = (String) (session.getAttribute("memberid") != null ? session
				.getAttribute("memberid").toString() : "");
		int pageno = Integer
				.parseInt(request.getParameter("page") != null ? request
						.getParameter("page") : "1");
		List<ThreadInfo> allrecords = ThreadInfoDAO.getallrecord(
				Integer.parseInt(memberid), pageno);
		
		request.setAttribute("allrecords", allrecords);
		request.setAttribute("pageno", pageno);
		String threadid = request.getParameter("threadid1") != null ? request
				.getParameter("threadid1") : "";
		String message_viewthreadinfo = request
				.getParameter("message_viewthreadinfo") != null ? request
				.getParameter("message_viewthreadinfo") : "";
		request.setAttribute("message_viewthreadinfo", message_viewthreadinfo);
		request.setAttribute("threadid", threadid);
		RequestDispatcher rd = request
				.getRequestDispatcher("viewthreadinfo.jsp");
		rd.forward(request, response);

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
