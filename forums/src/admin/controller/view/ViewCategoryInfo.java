package admin.controller.view;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.CategoryInfoDAO;
import model.to.CategoryInfo;

@WebServlet("/admin/viewcategoryinfo.do")
public class ViewCategoryInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request,response);
	}

	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<CategoryInfo> allrecords = CategoryInfoDAO.getallrecord();
		request.setAttribute("allrecords", allrecords);
		String categoryid = request.getParameter("categoryid1")!=null?request.getParameter("categoryid1"):"";		
		String message_viewcategory = request.getParameter("message_viewcategory")!=null?request.getParameter("message_viewcategory"):"";
		request.setAttribute("message_viewcategory", message_viewcategory);
		request.setAttribute("categoryid", categoryid);
		RequestDispatcher rd = request.getRequestDispatcher("viewcategoryinfo.jsp");
		rd.forward(request, response);
	}
}
