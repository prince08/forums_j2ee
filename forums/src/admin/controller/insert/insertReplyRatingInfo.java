package admin.controller.insert;

import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.MemberInfoDAO;
import model.dao.ReplyRatingInfoDAO;
import model.dao.ThreadReplyInfoDAO;
import model.to.ReplyRatingInfo;
import operations.Checks;

@WebServlet("/insertreplyratinginfo.do")
public class insertReplyRatingInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
			
		String message_rating = "";
		HttpSession session = request.getSession();
		String memberid = session.getAttribute("memberid").toString().trim();
		String thid1 = request.getParameter("thid1");
		String rating = request.getParameter("rating");
		String replyid = request.getParameter("replyid");
		if(thid1 != null && memberid != null && rating != null && replyid != null){
			if(Checks.isNumeric(thid1) && Checks.isNumeric(memberid) && Checks.isNumeric(rating) && Checks.isNumeric(replyid))
			{				
				ReplyRatingInfo data = new ReplyRatingInfo();
				
				data.setRating(Integer.parseInt(rating));
				data.setReplydate(new Date());
				data.setMemberinfo(MemberInfoDAO.getSinglerecord(Integer.parseInt(memberid)));
				data.setReplyinfo(ThreadReplyInfoDAO.getSinglerecord(Integer.parseInt(replyid)));
				
				if(ReplyRatingInfoDAO.insertRecord(data)){				
					message_rating = "Insertion happen.";					
				}else{
					message_rating = "Insertion not happen.";
				}
			}			
		}		
		request.setAttribute("thid1", thid1);
		request.setAttribute("message_rating", message_rating);
		RequestDispatcher rd = request.getRequestDispatcher("viewsinglethread.jsp");
		rd.forward(request, response);
		
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
