package admin.controller.insert;

import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.CategoryInfoDAO;
import model.dao.MemberInfoDAO;
import model.dao.ThreadInfoDAO;
import model.to.CategoryInfo;
import model.to.MemberInfo;
import model.to.ThreadInfo;
import operations.Checks;

@WebServlet("/member/insertthreadinfo.do")
public class insertThreadInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String threadtitle = request.getParameter("threadtitle") != null ? request
				.getParameter("threadtitle").trim() : "";
		String threadtext = request.getParameter("threadtext") != null ? request
				.getParameter("threadtext").trim() : "";
		String categoryid = request.getParameter("categoryid") != null ? request
				.getParameter("categoryid").trim() : "";
		HttpSession session = request.getSession();
		
		String memberid = session.getAttribute("memberid")!=null?session.getAttribute("memberid").toString():"";		
		
		String message_addthreadinfo = "";

		if (Checks.isEmpty(threadtitle) || Checks.isEmpty(threadtext)|| Checks.isEmpty(categoryid)||Checks.isEmpty(memberid)) {
			message_addthreadinfo = "Please fill all fields.";
		} else {
			
			MemberInfo data = new MemberInfo();
		if(Checks.isNumeric(memberid) && memberid!=null){	
			
			data = MemberInfoDAO.getSinglerecord(Integer.parseInt(memberid));
			if(data!=null){				
				ThreadInfo data1 = new ThreadInfo();
				data1.setMemberinfo(data);
				data1.setThreadtitle(threadtitle);
				data1.setThreadtext(threadtext);
				data1.setThreaddate(new Date());
				CategoryInfo data2 = CategoryInfoDAO.getSingleRecord(categoryid);
				data1.setCategoryinfo(data2);
				if(ThreadInfoDAO.insertRecord(data1)){
					message_addthreadinfo = "Insertion of the thread is succesfully happen.";
				}else{
					message_addthreadinfo = ThreadInfoDAO.getErrormessage();
				}				
			}else{
				message_addthreadinfo="member is not valid.";
			}
		  }else{
			  message_addthreadinfo = "memberid is not numeric & is "+memberid;		  
		  }
		}

		request.setAttribute("message_addthreadinfo", message_addthreadinfo);
		RequestDispatcher rd;
		if(message_addthreadinfo.equalsIgnoreCase("Insertion of the thread is succesfully happen.")){
			 rd = request.getRequestDispatcher("viewthreadinfo.do");
		}else{
			 rd = request
					.getRequestDispatcher("addthreadinfo.jsp");			
		}
		rd.forward(request, response);
	}
}
