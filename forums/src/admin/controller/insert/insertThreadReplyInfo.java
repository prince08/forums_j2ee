package admin.controller.insert;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.MemberInfoDAO;
import model.dao.ThreadInfoDAO;
import model.dao.ThreadReplyInfoDAO;
import model.to.MemberInfo;
import model.to.ThreadInfo;
import model.to.ThreadReplyInfo;

import operations.Checks;

/**
 * Servlet implementation class insertThreadReplyInfo
 */
@WebServlet("/insertthreadreplyinfo.do")
public class insertThreadReplyInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String threadid = request.getParameter("thid")!=null?request.getParameter("thid"):"";
		String memberid = session.getAttribute("memberid")!=null?session.getAttribute("memberid").toString():"";
		//System.out.println(memberid);
		String replytext = request.getParameter("replytext")!=null?request.getParameter("replytext"):"";
		String message_threadreply = "";
		//int thid;
		if(Checks.isEmpty(threadid)||Checks.isEmpty(memberid)||Checks.isEmpty(replytext)){
			message_threadreply = "Please write a comment first. threadid: "+threadid+" memberid: "+memberid+" reply: "+replytext;
		}else{
			if(Checks.isNumeric(memberid) && Checks.isNumeric(threadid)){
				MemberInfo memberinfo = MemberInfoDAO.getSinglerecord(Integer.parseInt(memberid));
				ThreadInfo threadinfo = ThreadInfoDAO.getSinglerecord(Integer.parseInt(threadid));
				//thid = threadinfo.getThreadid();
				if(memberinfo !=null && threadinfo !=null){
					ThreadReplyInfo data = new ThreadReplyInfo();
					data.setMemberinfo(memberinfo);
					data.setThreadinfo(threadinfo);
					data.setReplytext(replytext);
					data.setReplydate(new Date());
					//System.out.println(" yes");
					if(ThreadReplyInfoDAO.insertRecord(data)){
						message_threadreply = "Reply is done.";
					}else{
						message_threadreply = ThreadInfoDAO.getErrormessage();
					}
				}else{
					message_threadreply = "memberid or threadid is not valid.";
				}
			}else{
				message_threadreply = "memberid or threadid is not numeric.";
			}
		}
		
		request.setAttribute("message_threadreply", message_threadreply);
		request.setAttribute("thid", threadid);
		RequestDispatcher rd = request
				.getRequestDispatcher("viewsinglethread.jsp");
		rd.forward(request, response);		
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
