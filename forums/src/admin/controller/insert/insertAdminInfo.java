package admin.controller.insert;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.LoginInfoDAO;
import model.to.LoginInfo;
import operations.Checks;

@WebServlet("/admin/insertadmininfo.do")
public class insertAdminInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("username") != null ? request
				.getParameter("username").trim() : "";
		String password1 = request.getParameter("password1") != null ? request
				.getParameter("password1").trim() : "";
		String password2 = request.getParameter("password2") != null ? request
				.getParameter("password2").trim() : "";
		String rolename = request.getParameter("rolename") != null ? request
				.getParameter("rolename").trim() : "";
		String sq = request.getParameter("sq") != null ? request.getParameter(
				"sq").trim() : "";
		String sans = request.getParameter("sans") != null ? request
				.getParameter("sans").trim() : "";
		String phoneno = request.getParameter("phoneno") != null ? request
				.getParameter("phoneno") : "";
		String emailid = request.getParameter("emailid") != null ? request
				.getParameter("emailid").trim() : "";
		String membername = request.getParameter("membername") != null ? request
				.getParameter("membername").trim() : "";		

		String message_addlogininfo = "";

		if (Checks.isEmpty(username) || Checks.isEmpty(password1)
				|| Checks.isEmpty(password2) || Checks.isEmpty(rolename)
				|| Checks.isEmpty(sq) || Checks.isEmpty(sans)
				|| Checks.isEmpty(emailid) || Checks.isEmpty(phoneno)
				|| Checks.isEmpty(membername)) {
			message_addlogininfo = "Please fill all the fields completly.";
		} else if (Checks.isNumeric(phoneno)) {
			
			if (password1.equals(password2)) {
				
				LoginInfo data = new LoginInfo();
				data.setUsername(username);
				data.setPassword(password1);
				data.setRolename(rolename);
				data.setSq(sq);
				data.setSans(sans);
				data.setEmailid(emailid);

				if (LoginInfoDAO.insertRecord(data)) {					
						message_addlogininfo = "Insertion of the record is happen.";					
				} else {
					message_addlogininfo = LoginInfoDAO.getErrormessage();
				}
			} else {
				message_addlogininfo = "enter same password.";
			}
		} else {
			message_addlogininfo = "Please enter numeric value in phone number field.";
		}
		
		request.setAttribute("message_addlogininfo", message_addlogininfo);
		RequestDispatcher rd = request.getRequestDispatcher("addadmin.jsp");
		rd.forward(request, response);
	}

}
