package admin.controller.insert;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.CategoryInfoDAO;
import model.to.CategoryInfo;

import operations.Checks;

/**
 * Servlet implementation class insertCategoryInfo
 */
@WebServlet("/admin/insertcategoryinfo.do")
public class insertCategoryInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
   	{
   		String categoryid = request.getParameter("categoryid") !=null?request.getParameter("categoryid").trim():"";
   		String categoryname = request.getParameter("categoryname") !=null?request.getParameter("categoryname").trim():"";
   		String description = request.getParameter("description") !=null?request.getParameter("description"):"";
   		String message_addcategory = "";
   		if(Checks.isEmpty(categoryid)||Checks.isEmpty(categoryname)||Checks.isEmpty(description))
   		{
   			message_addcategory = "please fill all fields.";
   		}
   		else
   		{
   			CategoryInfo data = new CategoryInfo();
   			data.setCategoryid(categoryid);
   			data.setCategoryname(categoryname);
   			data.setDescription(description);
   			if(CategoryInfoDAO.insertRecord(data))
   			{
   				message_addcategory = "insertion happen.";
   			}
   			else
   			{
   				message_addcategory = CategoryInfoDAO.getErrormessage();
   			}
   		}
   		
   		request.setAttribute("message_addcategory", message_addcategory);
   		RequestDispatcher rd ;
   		if(message_addcategory.equalsIgnoreCase("insertion happen.")){
   			rd =request.getRequestDispatcher("viewcategoryinfo.do");
   		}else{
   			rd =request.getRequestDispatcher("addcategoryinfo.jsp");
   		}
   		rd.forward(request, response);
	}
}
