package admin.controller.insert;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.dao.MemberInfoDAO;
import model.dao.MemberPicsDAO;
import model.to.MemberInfo;
import model.to.MemberPics;

@WebServlet("/member/insertmemberpics.do")
@MultipartConfig
public class insertMemberPics extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String message_pic = "";
		HttpSession session = request.getSession();
		String memberid = session.getAttribute("memberid") != null ? session
				.getAttribute("memberid").toString() : "";
		MemberInfo memberinfo = MemberInfoDAO.getSinglerecord(Integer
				.parseInt(memberid));	
		
		Part p1 = request.getPart("file");
		
		if (p1 != null && memberinfo != null) {
	
			if (p1.getInputStream() instanceof FileInputStream) {

				String value = p1.getHeader("content-disposition");
				int index = value.lastIndexOf('=');
				value = value.substring(index + 1);
				value = value.replace("\"", "");
				String picname = value;
				String extname = picname
						.substring(picname.lastIndexOf(".") + 1);

				MemberPics data = new MemberPics();
				data.setPicname(picname);
				data.setExtname(extname);
				data.setPicsize(p1.getInputStream().available());
				data.setPictype(p1.getContentType());
				data.setMemberinfo(memberinfo);
				int picid = MemberPicsDAO.insertRecord(data);
				if (picid != 0) {
					String path = "upload/" + picid + "." + extname;
					path = getServletContext().getRealPath(path);
					FileOutputStream fout = new FileOutputStream(path);
					InputStream is = p1.getInputStream();
					int i = is.read();
					while (i != -1) {
						fout.write(i);
						i = is.read();
					}
					fout.close();
					message_pic = "Photo is uploaded successfully";
				} else {
					message_pic = "Photo is not uploaded.";
				}
			} else {
				message_pic = "Please select some value.";
			}
		}
		request.setAttribute("message_pic", message_pic);
		RequestDispatcher rd = request.getRequestDispatcher("addmemberpic.jsp");
		rd.forward(request, response);
	}

}
