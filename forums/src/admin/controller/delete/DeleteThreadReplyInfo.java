package admin.controller.delete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.ReplyRatingInfoDAO;
import model.dao.ThreadReplyInfoDAO;

import operations.Checks;

@WebServlet("/deletethreadreplyinfo.do")
public class DeleteThreadReplyInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message_deletereply = "";
		String repid = request.getParameter("replyid");
		String thid1 = request.getParameter("thid1");
		
		if(repid != null && Checks.isNumeric(repid)){
			int replyid = Integer.parseInt(repid);
			if(ReplyRatingInfoDAO.deleteRecord(replyid) > 0){
				if(ThreadReplyInfoDAO.deleteRecord(replyid)){
					message_deletereply = "deletion happen";
				}else{
					message_deletereply = "deletion not happen.";
				}
			}else if(ReplyRatingInfoDAO.deleteRecord(replyid) == 0){
				if(ThreadReplyInfoDAO.deleteRecord(replyid)){
					message_deletereply = "deletion happen";
				}else{
					message_deletereply = "deletion not happen.";
				}
			}
		}				
		request.setAttribute("thid1", thid1);
		request.setAttribute("message_deletereply", message_deletereply);
		RequestDispatcher rd = request.getRequestDispatcher("viewsinglethread.jsp");
		rd.forward(request, response);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
