package admin.controller.delete;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.CategoryInfoDAO;
import model.dao.ReplyRatingInfoDAO;
import model.dao.ThreadInfoDAO;
import model.dao.ThreadReplyInfoDAO;
import model.to.ThreadInfo;
import model.to.ThreadReplyInfo;

@WebServlet("/admin/deletecategoryinfo.do")
public class DeleteCategoryInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("categoryid2") != null) {
			String categoryid = request.getParameter("categoryid2").trim();

			List<ThreadInfo> threadrecords = ThreadInfoDAO
					.getallrecordByCategoryID(categoryid);
			List<ThreadReplyInfo> threadReplyrecords;

			int delthreadreply = 0;
			int delreplyrating = 0;
			int delthreads = 0;
			if (threadrecords != null && threadrecords.size() > 0) {

				for (ThreadInfo o : threadrecords) {

					threadReplyrecords = ThreadReplyInfoDAO
							.getallrecordOrderByReplyId(o.getThreadid());
					if (threadReplyrecords != null
							&& threadReplyrecords.size() > 0) {

						for (ThreadReplyInfo d : threadReplyrecords) {

							delreplyrating += ReplyRatingInfoDAO.deleteRecord(d
									.getReplyid());
						}
						delthreadreply += ThreadReplyInfoDAO
								.deleteRecordsbyThreadID(o.getThreadid());
					}
				}
				delthreads = ThreadInfoDAO
						.deleteRecordsByCategoryID(categoryid);
			}

			if (delreplyrating >= 0) {
				if (delthreadreply >= 0) {
					if (delthreads >= 0) {
						if (CategoryInfoDAO.deleteRecord(categoryid)) {
							request.setAttribute("message_viewcategory",
									"Deletion Happen");
						} else {
							request.setAttribute("message_viewcategory",
									CategoryInfoDAO.getErrormessage());
						}
					}
				}
			}

		}
		RequestDispatcher rd = request
				.getRequestDispatcher("viewcategoryinfo.do");
		rd.forward(request, response);
	}

}
