package admin.controller.delete;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.ThreadInfoDAO;
import model.dao.ThreadReplyInfoDAO;

/**
 * Servlet implementation class DeleteThreadInfo
 */
@WebServlet("/member/deletethreadinfo.do")
public class DeleteThreadInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("threadid2") != null) {
			String threadid = request.getParameter("threadid2").trim();

			if (ThreadReplyInfoDAO.deleteRecordsbyThreadID(Integer
					.parseInt(threadid)) > 0
					|| ThreadInfoDAO.deleteRecord(Integer.parseInt(threadid))) {
				request.setAttribute("message_viewthreadinfo",
						"Deletion Happen");
			} else {
				request.setAttribute("message_viewthreadinfo",
						ThreadInfoDAO.getErrormessage());
			}
		}

		RequestDispatcher rd = request
				.getRequestDispatcher("viewthreadinfo.do");
		rd.forward(request, response);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
