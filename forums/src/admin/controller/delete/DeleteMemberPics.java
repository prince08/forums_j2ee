package admin.controller.delete;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.MemberPicsDAO;
import model.to.MemberPics;

/**
 * Servlet implementation class DeleteMemberPics
 */
@WebServlet("/member/deletememberpics.do")
public class DeleteMemberPics extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message_deletepic = "";
		String pid = request.getParameter("picid");
		if (pid != null) {
			int picid = Integer.parseInt(pid);
			MemberPics pic = MemberPicsDAO.getSinglerecord(picid);
			String picname = pic.getPicid() + "." + pic.getExtname();
			String path = getServletContext().getRealPath("/upload/" + picname);
			File file = new File(path);
			if (file.exists()) {
				if (MemberPicsDAO.deleteRecord(picid)) {
					if (file.delete()) {
						message_deletepic = "deletion happen.";
					}
				} else {
					message_deletepic = "deletion is not done.";
				}
			}else{
				if (MemberPicsDAO.deleteRecord(picid)) {
					if (file.delete()) {
						message_deletepic = "deletion happen.";
					}
				} else {
					message_deletepic = "deletion is not done.";
				}
			}
		}
		request.setAttribute("message_deletepic", message_deletepic);
		RequestDispatcher rd = request.getRequestDispatcher("addmemberpic.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
