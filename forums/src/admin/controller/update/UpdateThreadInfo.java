package admin.controller.update;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.LoginInfoDAO;
import model.dao.ThreadInfoDAO;
import model.to.ThreadInfo;
import operations.Checks;

/**
 * Servlet implementation class UpdateThreadInfo
 */
@WebServlet("/member/updatethreadinfo.do")
public class UpdateThreadInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String threadid = request.getParameter("threadid");
		String threadtitle = request.getParameter("threadtitle");
		String threadtext = request.getParameter("threadtext");
		String message_viewthreadinfo = "";

		if (threadid != null && threadtitle != null && threadtext != null) {
			if (Checks.isEmpty(threadid) || Checks.isEmpty(threadtitle)
					|| Checks.isEmpty(threadtext)) {
				message_viewthreadinfo = "Please fill all the fields completly.";
			} else {
				ThreadInfo data = new ThreadInfo();
				data = ThreadInfoDAO.getSinglerecord(Integer.parseInt(threadid));
				if(data!=null){
				data.setThreadtitle(threadtitle);
				data.setThreadtext(threadtext);								
				if (ThreadInfoDAO.updateRecord(data)) {
					message_viewthreadinfo = "Updation of the record is happen.";
				} else {
					message_viewthreadinfo = LoginInfoDAO.getErrormessage();
				}
				}else
				{
					message_viewthreadinfo = "Record doesnot exist.";
				}
			}
		}
		request.setAttribute("message_viewthreadinfo", message_viewthreadinfo);
		RequestDispatcher rd = request
				.getRequestDispatcher("viewthreadinfo.do");
		rd.forward(request, response);

	}
}
