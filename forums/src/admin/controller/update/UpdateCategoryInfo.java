package admin.controller.update;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.CategoryInfoDAO;
import model.dao.LoginInfoDAO;
import model.to.CategoryInfo;
import operations.Checks;

/**
 * Servlet implementation class UpdateCategoryInfo
 */
@WebServlet("/admin/updatecategoryinfo.do")
public class UpdateCategoryInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryid = request.getParameter("categoryid") != null ? request
				.getParameter("categoryid").trim() : "";
		String categoryname = request.getParameter("categoryname") != null ? request
				.getParameter("categoryname").trim() : "";
		String description = request.getParameter("description") != null ? request
				.getParameter("description").trim() : "";
		
		String message_viewcategory = "";
				
		if (Checks.isEmpty(categoryid) || Checks.isEmpty(categoryname)
				|| Checks.isEmpty(description)) 
		{
			message_viewcategory = "Please fill all the fields completly.";	
		} 
		else 
		{		
			CategoryInfo data = new CategoryInfo();
			data.setCategoryid(categoryid);
			data.setCategoryname(categoryname);
			data.setDescription(description);
			if(CategoryInfoDAO.updatecategoryinfo(data))
			{
				message_viewcategory = "Updation of the record is happen.";
			}
			else
			{
				message_viewcategory = LoginInfoDAO.getErrormessage();
			}
		}
		
		request.setAttribute("message_viewcategory", message_viewcategory);		
		RequestDispatcher rd = request.getRequestDispatcher("viewcategoryinfo.do");
		rd.forward(request, response);

	}

}
