package model.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.*;
import model.to.LoginInfo;


public class LoginInfoDAO 
{
	private static String errorMessage;
	
	public static String getErrormessage()
	{
		return errorMessage;
	}
	public static boolean insertRecord(LoginInfo data)
	{
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			return true;			
		}
		catch(Exception ex)
		{
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public static LoginInfo checklogin(String username)
	{
		try{
			
			Session session = HibernateUtil.getSession();			
			
			LoginInfo data = (LoginInfo) session.get(LoginInfo.class, username);			
			
			return data;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			//System.out.println("   i m in logininfo dao  ");
			return null;
		}
	}
	
	public static boolean updatelogin(LoginInfo data)
	{
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.merge(data);			
			session.getTransaction().commit();
			return true;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			return false;
		}
	}

	public static List<LoginInfo> getallrecord()
	{
		try{
			List<LoginInfo> alllogininfo = null;
			Session session = HibernateUtil.getSession();			
			Query query = session.createQuery("from LoginInfo");
			List<?> allValues = query.list();
			if(allValues!=null && allValues.size()>0){
				alllogininfo = new ArrayList<LoginInfo>();
				for(Object o : allValues){
					LoginInfo data = (LoginInfo) o;
					alllogininfo.add(data);
				}
			}
			return alllogininfo;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			return null;
		}
	}
	
	public static LoginInfo getSinglerecord(String username) {
		try {

			Session session = HibernateUtil.getSession();			
			LoginInfo data = (LoginInfo) session.get(LoginInfo.class, username);			
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			return null;
		}
	}
}
