package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.to.MemberPics;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

public class MemberPicsDAO {
	private static String errorMessage;

	public static String getErrormessage() {
		return errorMessage;
	}

	public static int insertRecord(MemberPics data) {
		try {
			int picid = 0;
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			Query q = session.createSQLQuery(
					"select seq_picid.currval as num from dual").addScalar(
					"num", Hibernate.INTEGER);
			picid=Integer.parseInt(q.uniqueResult().toString());
			return picid;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return 0;
		}
	}

	public static List<MemberPics> getallrecord() {
		try {
			List<MemberPics> all = null;
			Session session = HibernateUtil.getSession();
			Query query = session.createQuery("from MemberPics");
			List<?> allValues = query.list();
			if (allValues != null && allValues.size() > 0) {
				all = new ArrayList<MemberPics>();
				for (Object o : allValues) {
					MemberPics data = (MemberPics) o;
					all.add(data);
				}
			}
			return all;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return null;
		}
	}
	
	public static List<MemberPics> getallrecord(int memberid)
	{
		try {
						
			Session session = HibernateUtil.getSession();
			
			String query="from MemberPics where memberid=:id order by picid desc";
			Query q = session.createQuery(query);
			q.setInteger("id", memberid);			
			List<?> allinfo = q.list();
			List<MemberPics> data = null;
			if(allinfo!=null && allinfo.size() > 0){
				data = new ArrayList<MemberPics>();
				for (Object o : allinfo) {
					MemberPics data1 = (MemberPics) o;
					data.add(data1);
				}
			}
			
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();			
			return null;
		}
	}

	public static MemberPics getSinglerecord(int picid) {
		try {

			Session session = HibernateUtil.getSession();			
			MemberPics data = (MemberPics) session.get(MemberPics.class, picid);
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return null;
		}
	}

	public static boolean deleteRecord(int picid) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			MemberPics data = (MemberPics) session.get(MemberPics.class, picid);
			session.delete(data);								
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
}
