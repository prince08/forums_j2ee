package model.dao;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtil 
{
	private HibernateUtil(){}
	
	private static SessionFactory factory;
	private static Session session;
	
	public static Session getSession()
	{
		try{				
			if(factory==null)
			{			
				factory = new AnnotationConfiguration().configure("hibernate.cfg.xml").buildSessionFactory();
				session = factory.openSession();
			}
			else if(session==null)
				{
					session = factory.openSession();
				}
			return session;
		}
		catch (Exception ex)
		{	
			ex.printStackTrace();
			return null;
		}
	}
	
	public static void closeSession()
	{
		session.close();
	}
	
}
