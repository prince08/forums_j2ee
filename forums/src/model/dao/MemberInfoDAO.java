package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.to.MemberInfo;
import org.hibernate.Query;
import org.hibernate.Session;

public class MemberInfoDAO {

	private static String errorMessage;

	public static String getErrormessage() {
		return errorMessage;
	}

	public static boolean insertRecord(MemberInfo data) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}

	public static List<MemberInfo> getallrecord() {
		try {
			List<MemberInfo> allmemberinfo = null;
			Session session = HibernateUtil.getSession();
			Query query = session.createQuery("from MemberInfo");
			List<?> allValues = query.list();
			if (allValues != null && allValues.size() > 0) {
				allmemberinfo = new ArrayList<MemberInfo>();
				for (Object o : allValues) {
					MemberInfo data = (MemberInfo) o;
					allmemberinfo.add(data);
				}
			}
			return allmemberinfo;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return null;
		}
	}

	public static MemberInfo getSinglerecord(int memberid) {
		try {

			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			MemberInfo data = (MemberInfo) session.get(MemberInfo.class, memberid);
			session.getTransaction().commit();
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			return null;
		}
	}
	
	public static MemberInfo getRecords(String username) {
		try {

			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			String query="from MemberInfo where username=:un";
			Query q = session.createQuery(query);
			q.setString("un", username);			
			List<?> allinfo = q.list();
			MemberInfo data = null;
			if(allinfo!=null && allinfo.size() > 0){
				if(allinfo.get(0) instanceof MemberInfo){
					data = (MemberInfo) allinfo.get(0);
				}
			}
			session.getTransaction().commit();
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			// System.out.println("   i m in logininfo dao  ");
			return null;
		}
	}
}
