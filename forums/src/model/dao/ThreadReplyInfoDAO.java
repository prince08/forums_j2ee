package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.to.ThreadReplyInfo;
import org.hibernate.Query;
import org.hibernate.Session;

public class ThreadReplyInfoDAO {
	private static String errorMessage;

	public static String getErrormessage() {
		return errorMessage;
	}
	
	public static boolean insertRecord(ThreadReplyInfo data)
	{
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			return true;			
		}
		catch(Exception ex)
		{
			errorMessage = ex.getMessage();
			//ex.printStackTrace();
			return false;
		}
	}
	
	public static boolean updateRecord(ThreadReplyInfo data)
	{
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.merge(data);			
			session.getTransaction().commit();
			return true;
		}catch(Exception ex){
			errorMessage = ex.getMessage();			
			return false;
		}
	}
	
	public static List<ThreadReplyInfo> getallrecordOrderByReplyId(int thid)
	{
		try{			
			Session session = HibernateUtil.getSession();
			
			String query="from ThreadReplyInfo where threadid=:thid order by replyid";
			Query q = session.createQuery(query);
			q.setInteger("thid", thid);
			List<?> allinfo = q.list();
			List<ThreadReplyInfo> data = null;
			if(allinfo!=null && allinfo.size() > 0){
				data = new ArrayList<ThreadReplyInfo>();
				for (Object o : allinfo) {
					ThreadReplyInfo data1 = (ThreadReplyInfo) o;
					data.add(data1);
				}
			}
			
			return data;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			return null;
		}
	}
	
	public static ThreadReplyInfo getSinglerecord(int replyid) {
		try {

			Session session = HibernateUtil.getSession();			
			ThreadReplyInfo data = (ThreadReplyInfo) session.get(ThreadReplyInfo.class, replyid);			
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			//System.out.println(errorMessage);
			return null;
		}
	}
	
	public static boolean deleteRecord(int replyid){
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			ThreadReplyInfo data = (ThreadReplyInfo) session.get(ThreadReplyInfo.class,replyid);			
			session.delete(data);
			session.getTransaction().commit();
			return true;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			//ex.printStackTrace();
			return false;
		}
	}
	
	public static int deleteRecordsbyThreadID(int thid)
	{
		try{			
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			String query=" delete from ThreadReplyInfo where threadid=:thid";
			Query q = session.createQuery(query);
			q.setInteger("thid", thid);
			int result = q.executeUpdate();			
			session.getTransaction().commit();
			return result;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			return 0;
		}
	}
}
