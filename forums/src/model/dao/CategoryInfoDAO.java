package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.to.CategoryInfo;
import org.hibernate.Query;
import org.hibernate.Session;

public class CategoryInfoDAO 
{
	private static String errorMessage;
	
	public static String getErrormessage()
	{
		return errorMessage;
	}
	public static boolean insertRecord(CategoryInfo data)
	{
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			return true;			
		}
		catch(Exception ex)
		{
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public static boolean updatecategoryinfo(CategoryInfo data)
	{
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.merge(data);			
			session.getTransaction().commit();
			return true;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			return false;
		}
	}

	public static List<CategoryInfo> getallrecord()
	{		
		try{
			List<CategoryInfo> allcategoryinfo = null;
			Session session = HibernateUtil.getSession();			
			Query query = session.createQuery("from CategoryInfo");
			List<?> allValues = query.list();
			if(allValues!=null && allValues.size()>0){
				allcategoryinfo = new ArrayList<CategoryInfo>();
				for(Object o : allValues)
				{
					CategoryInfo data = (CategoryInfo) o;
					allcategoryinfo.add(data);
				}
			}
			return allcategoryinfo;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			return null;
		}
	}

	public static boolean deleteRecord(String categoryid){
		try{
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			CategoryInfo data = (CategoryInfo) session.get(CategoryInfo.class,categoryid);			
			session.delete(data);
			session.getTransaction().commit();
			return true;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			ex.printStackTrace();
			return false;
		}
	}
	
	public static CategoryInfo getSingleRecord(String categoryid)
	{
		try{
			
			Session session = HibernateUtil.getSession();			
			session.beginTransaction();
			CategoryInfo data = (CategoryInfo) session.get(CategoryInfo.class, categoryid);			
			session.getTransaction().commit();
			return data;
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			//System.out.println("   i m in logininfo dao  ");
			return null;
		}
	}
	
}