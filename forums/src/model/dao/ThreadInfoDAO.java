package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.to.ThreadInfo;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

public class ThreadInfoDAO {

	private static String errorMessage;

	public static String getErrormessage() {
		return errorMessage;
	}

	public static boolean insertRecord(ThreadInfo data) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}

	public static boolean updateRecord(ThreadInfo data) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.merge(data);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}

	public static int getNumberOfRecord() {
		int num = 0;
		Session session = HibernateUtil.getSession();
		String qry = "select count(*) from threadinfo";
		SQLQuery q = session.createSQLQuery(qry);
		num = Integer.parseInt(q.uniqueResult().toString());
		return num;
	}

	public static int getNumberOfrecord(int membertid) {
		int num = 0;
		Session session = HibernateUtil.getSession();
		String qry = "select count(*) from threadinfo where memberid='"
				+ membertid + "'";
		SQLQuery q = session.createSQLQuery(qry);
		num = Integer.parseInt(q.uniqueResult().toString());
		return num;
	}

	public static int getNumberOfrecord(String categoryid) {
		int num = 0;
		Session session = HibernateUtil.getSession();
		String qry = "select count(*) from threadinfo where categoryid='"
				+ categoryid + "'";
		SQLQuery q = session.createSQLQuery(qry);
		num = Integer.parseInt(q.uniqueResult().toString());
		return num;
	}

	public static List<ThreadInfo> getallrecord(int memberid, int pageno) {
		try {

			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			String query = "from ThreadInfo where memberid=:id order by threadid desc";
			Query q = session.createQuery(query);
			q.setInteger("id", memberid);
			int end = (5 * pageno);
			int start = end - 5;
			q.setFirstResult(start);
			q.setMaxResults(5);
			List<?> allinfo = q.list();
			List<ThreadInfo> data = null;
			if (allinfo != null && allinfo.size() > 0) {
				data = new ArrayList<ThreadInfo>();
				for (Object o : allinfo) {
					ThreadInfo data1 = (ThreadInfo) o;
					data.add(data1);
				}
			}
			session.getTransaction().commit();
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			// System.out.println("   i m in logininfo dao  ");
			return null;
		}
	}

	public static List<ThreadInfo> getallrecordOrderByThreadId(int pageno) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			String query = "from ThreadInfo order by threadid desc";
			Query q = session.createQuery(query);
			int end = (5 * pageno);
			int start = end - 5;
			q.setFirstResult(start);
			q.setMaxResults(5);
			List<?> allinfo = q.list();
			List<ThreadInfo> data = null;
			if (allinfo != null && allinfo.size() > 0) {
				data = new ArrayList<ThreadInfo>();
				for (Object o : allinfo) {
					ThreadInfo data1 = (ThreadInfo) o;
					data.add(data1);
				}
			}
			session.getTransaction().commit();
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			ex.printStackTrace();
			return null;
		}
	}

	public static List<ThreadInfo> getallrecordOrderByThreadId(
			String categoryid, int pageno) {
		try {
			Session session = HibernateUtil.getSession();
			String query = "from ThreadInfo where categoryid=:cid order by threadid desc";
			Query q = session.createQuery(query);
			q.setString("cid", categoryid);
			int end = (5 * pageno);
			int start = end - 5;
			q.setFirstResult(start);
			q.setMaxResults(5);
			List<?> allinfo = q.list();
			List<ThreadInfo> data = null;
			if (allinfo != null && allinfo.size() > 0) {
				data = new ArrayList<ThreadInfo>();
				for (Object o : allinfo) {
					ThreadInfo data1 = (ThreadInfo) o;
					data.add(data1);
				}
			}
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return null;
		}
	}

	public static List<ThreadInfo> getallrecordByCategoryID(String categoryid) {
		try {
			Session session = HibernateUtil.getSession();
			String query = "from ThreadInfo where categoryid=:cid";
			Query q = session.createQuery(query);
			q.setString("cid", categoryid);
			List<?> allinfo = q.list();
			List<ThreadInfo> data = null;
			if (allinfo != null && allinfo.size() > 0) {
				data = new ArrayList<ThreadInfo>();
				for (Object o : allinfo) {
					ThreadInfo data1 = (ThreadInfo) o;
					data.add(data1);
				}
			}
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return null;
		}
	}

	public static ThreadInfo getSinglerecord(int threadid) {
		try {

			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			ThreadInfo data = (ThreadInfo) session.get(ThreadInfo.class,
					threadid);
			session.getTransaction().commit();
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			System.out.println(errorMessage);
			return null;
		}
	}

	public static List<ThreadInfo> getSearchResult(String searchtext) {
		try {
			Session session = HibernateUtil.getSession();
			String sql = "select * from ThreadInfo where dbms_lob.instr(lower(threadtext) || lower(threadtitle), '"
					+ searchtext.toLowerCase()
					+ "',1,1)>0  or memberid in ( select memberid from MemberInfo where lower(MemberName) like '%"
					+ searchtext.toLowerCase() + "%' )";
			// System.out.println(sql);
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(ThreadInfo.class);
			List<?> allinfo = query.list();
			// System.out.println(allinfo);
			List<ThreadInfo> data = null;
			if (allinfo != null && allinfo.size() > 0) {
				data = new ArrayList<ThreadInfo>();
				for (Object o : allinfo) {
					ThreadInfo data1 = (ThreadInfo) o;
					data.add(data1);
				}
			}
			return data;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			// ex.printStackTrace();
			return null;
		}
	}

	public static boolean deleteRecord(int threadid) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			ThreadInfo data = (ThreadInfo) session.get(ThreadInfo.class,
					threadid);
			session.delete(data);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			ex.printStackTrace();
			return false;
		}
	}

	public static int deleteRecordsByCategoryID(String categoryid) {
		try {
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			String sql = "delete from ThreadInfo where categoryid=:cid";
			SQLQuery query = session.createSQLQuery(sql);
			query.setString("cid", categoryid);
			int result = query.executeUpdate();
			session.getTransaction().commit();
			return result;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			ex.printStackTrace();
			return 0;
		}
	}
	
}
