package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.to.ReplyRatingInfo;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;


public class ReplyRatingInfoDAO {
	
	private static String errorMessage;

	public static String getErrormessage() {
		return errorMessage;
	}

	public static boolean insertRecord(ReplyRatingInfo data) {
		try {			
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			session.save(data);
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public static List<ReplyRatingInfo> isRecord(int replyid, int memberid){
		try{
			Session session = HibernateUtil.getSession();
			String q = " select * from replyratinginfo where replyid='"+replyid+"' and memberid='"+memberid+"'";		
			SQLQuery query = session.createSQLQuery(q);
			query.addEntity(ReplyRatingInfo.class);
			List<?> allinfo = query.list();
			List<ReplyRatingInfo> data = null;
			if(allinfo!=null && allinfo.size() > 0){
				data = new ArrayList<ReplyRatingInfo>();
				for (Object o : allinfo) {
					ReplyRatingInfo data1 = (ReplyRatingInfo) o;
					data.add(data1);
				}
			}	
			return data;
		}catch(Exception ex){
			 errorMessage = ex.getMessage();
			 //System.out.println(ex.getMessage()+ "  "+ " hello 2");
			 return null;			 
		}
	}
	
	public static float getAverage(int replyid){
		try{			
			float avg = 0;
			Session session = HibernateUtil.getSession();
			String q = "select avg(rating) as num from ReplyRatingInfo where replyid="+replyid;			
			SQLQuery query = session.createSQLQuery(q).addScalar("num", Hibernate.FLOAT);								
			avg = Float.parseFloat(query.uniqueResult().toString());			
			return avg;
		}catch(Exception ex){			
			errorMessage = ex.getMessage();		
			return 0;
		}
	}
	
	public static int deleteRecord(int replyid){
		try{			
			Session session = HibernateUtil.getSession();
			session.beginTransaction();
			String query=" delete from ReplyRatingInfo where replyid=:rid";
			Query q = session.createQuery(query);
			q.setInteger("rid", replyid);
			int result = q.executeUpdate();			
			session.getTransaction().commit();
			return result;			
		}catch(Exception ex){
			errorMessage = ex.getMessage();
			return 0;
		}
	}

}
