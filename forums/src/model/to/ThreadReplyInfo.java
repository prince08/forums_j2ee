package model.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="threadreplyinfo")
public class ThreadReplyInfo implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="seq_replyid")
	@SequenceGenerator(name="seq_replyid",sequenceName="seq_replyid",initialValue=1,allocationSize=1)
	@Column(name="replyid")
	private int replyid;
	@Column(name="replytext")
	private String replytext;
	@Column(name="replydate")
	private Date replydate;
	
	@ManyToOne
	@JoinColumn(name="threadid")
	private ThreadInfo threadinfo;
	
	@ManyToOne
	@JoinColumn(name="memberid")
	private MemberInfo memberinfo;

	public int getReplyid() {
		return replyid;
	}

	public void setReplyid(int replyid) {
		this.replyid = replyid;
	}

	public String getReplytext() {
		return replytext;
	}

	public void setReplytext(String replytext) {
		this.replytext = replytext;
	}

	public Date getReplydate() {
		return replydate;
	}

	public void setReplydate(Date replydate) {
		this.replydate = replydate;
	}

	public ThreadInfo getThreadinfo() {
		return threadinfo;
	}

	public void setThreadinfo(ThreadInfo threadinfo) {
		this.threadinfo = threadinfo;
	}

	public MemberInfo getMemberinfo() {
		return memberinfo;
	}

	public void setMemberinfo(MemberInfo memberinfo) {
		this.memberinfo = memberinfo;
	}
	
	

}
