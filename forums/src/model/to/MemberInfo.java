package model.to;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="memberinfo")
public class MemberInfo implements Serializable{
	@Transient
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="seq_memberid")
	@SequenceGenerator(name="seq_memberid",sequenceName="seq_memberid",initialValue=1,allocationSize=1)
	@Column(name="memberid")
	private int memberid;
	@Column(name="membername")
	private String membername;
	@Column(name="dob")
	private Date dob;
	@Column(name="createdate")
	private Date createdate;
	@Column(name="phoneno")
	private String phoneno;
	
	@OneToOne()
	@JoinColumn(name="username")
	private LoginInfo logininfo;
	
	public LoginInfo getLogininfo() {
		return logininfo;
	}
	public void setLogininfo(LoginInfo logininfo) {
		this.logininfo = logininfo;
	}
	
	public int getMemberid() {
		return memberid;
	}
	public void setMemberid(int memberid) {
		this.memberid = memberid;
	}
	public String getMembername() {
		return membername;
	}
	public void setMembername(String membername) {
		this.membername = membername;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
