package model.to;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="replyratinginfo")
public class ReplyRatingInfo implements Serializable{
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="seq_rrid")
	@SequenceGenerator(name="seq_rrid",sequenceName="seq_rrid",initialValue=1,allocationSize=1)
	@Column(name="rrid")
	private int rrid;
	@Column(name="rating")
	private int rating;
	@Column(name="ratingdate")
	private Date replydate;
	@ManyToOne
	@JoinColumn(name="memberid")
	private MemberInfo memberinfo;
	@ManyToOne
	@JoinColumn(name="replyid")
	private ThreadReplyInfo replyinfo;
	public int getRrid() {
		return rrid;
	}
	public void setRrid(int rrid) {
		this.rrid = rrid;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Date getReplydate() {
		return replydate;
	}
	public void setReplydate(Date replydate) {
		this.replydate = replydate;
	}
	public MemberInfo getMemberinfo() {
		return memberinfo;
	}
	public void setMemberinfo(MemberInfo memberinfo) {
		this.memberinfo = memberinfo;
	}
	public ThreadReplyInfo getReplyinfo() {
		return replyinfo;
	}
	public void setReplyinfo(ThreadReplyInfo replyinfo) {
		this.replyinfo = replyinfo;
	}
	
	

}
