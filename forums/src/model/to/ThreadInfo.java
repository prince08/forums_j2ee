package model.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="threadinfo")
public class ThreadInfo implements Serializable{
	@Transient
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator="seq_threadid")
	@SequenceGenerator(name="seq_threadid",sequenceName="seq_threadid",initialValue=1,allocationSize=1)
	@Column(name="threadid")
	private int threadid;
	@Column(name="threadtitle")
	private String threadtitle;
	@Column(name="threadtext")
	private String threadtext;
	@Column(name="threaddate")
	private Date threaddate;
	@Column(name="totalvisitor")
	private int totalvisitor;
	
	@ManyToOne
	@JoinColumn(name="memberid")
	private MemberInfo memberinfo;
	
	@ManyToOne
	@JoinColumn(name="categoryid")
	private CategoryInfo categoryinfo;
	
	public MemberInfo getMemberinfo() {
		return memberinfo;
	}
	public void setMemberinfo(MemberInfo memberinfo) {
		this.memberinfo = memberinfo;
	}
	public CategoryInfo getCategoryinfo() {
		return categoryinfo;
	}
	public void setCategoryinfo(CategoryInfo categoryinfo) {
		this.categoryinfo = categoryinfo;
	}
	public int getThreadid() {
		return threadid;
	}
	public void setThreadid(int threadid) {
		this.threadid = threadid;
	}
	public String getThreadtitle() {
		return threadtitle;
	}
	public void setThreadtitle(String threadtitle) {
		this.threadtitle = threadtitle;
	}
	public String getThreadtext() {
		return threadtext;
	}
	public void setThreadtext(String threadtext) {
		this.threadtext = threadtext;
	}
	public Date getThreaddate() {
		return threaddate;
	}
	public void setThreaddate(Date threaddate) {
		this.threaddate = threaddate;
	}
	public int getTotalvisitor() {
		return totalvisitor;
	}
	public void setTotalvisitor(int totalvisitor) {
		this.totalvisitor = totalvisitor;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
