package model.to;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="memberpics")
public class MemberPics implements Serializable{
	@Transient
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="seq_picid")
	@SequenceGenerator(name="seq_picid",sequenceName="seq_picid",initialValue=1,allocationSize=1)
	@Column(name="picid")
	private int picid;
	
	@Column(name="picname")
	private String picname;
	
	@Column(name="extname")
	private String extname;
	
	@Column(name="picsize")
	private int picsize;
	
	@Column(name="pictype")
	private String pictype;
	
	@OneToOne
	@JoinColumn(name="memberid")
	private MemberInfo memberinfo;
	public int getPicid() {
		return picid;
	}
	public void setPicid(int picid) {
		this.picid = picid;
	}
	public String getPicname() {
		return picname;
	}
	public void setPicname(String picname) {
		this.picname = picname;
	}
	public String getExtname() {
		return extname;
	}
	public void setExtname(String extname) {
		this.extname = extname;
	}
	public int getPicsize() {
		return picsize;
	}
	public void setPicsize(int picsize) {
		this.picsize = picsize;
	}
	public String getPictype() {
		return pictype;
	}
	public void setPictype(String pictype) {
		this.pictype = pictype;
	}
	public MemberInfo getMemberinfo() {
		return memberinfo;
	}
	public void setMemberinfo(MemberInfo memberinfo) {
		this.memberinfo = memberinfo;
	}

}
