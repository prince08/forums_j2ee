package model.to;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="logininfo")
public class LoginInfo implements Serializable{
	@Transient
	
	private static final long serialVersionUID = 1L;
	
	//@Transient
	//private static final long serialVersionUID = 1L;
	@Id
	private String username;
	@Column(name="password")
	private String password;
	@Column(name="rolename")
	private String rolename;
	@Column(name="sq")
	private String sq;
	@Column(name="sans")
	private String sans;
	@Column(name="emailid")
	private String emailid;
	@Column(name="lastlogin")
	private Date lastlogin;
	
	public Date getLastlogin() {
		return lastlogin;
	}
	public void setLastlogin(Date lastlogin) {
		this.lastlogin = lastlogin;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getSq() {
		return sq;
	}
	public void setSq(String sq) {
		this.sq = sq;
	}
	public String getSans() {
		return sans;
	}
	public void setSans(String sans) {
		this.sans = sans;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	
}
