package controller.fetch;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dao.LoginInfoDAO;
import model.to.LoginInfo;
import operations.Checks;
import operations.RandomString;
import operations.Utility;

/**
 * Servlet implementation class ForgotPassword
 */
@WebServlet("/forgotpassword.do")
public class ForgotPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
			
		String username = request.getParameter("username")!=null?request.getParameter("username"):"";
		String sq = request.getParameter("sq")!=null? request.getParameter("sq"):"";
		String sans = request.getParameter("sans")!=null? request.getParameter("sans"):"";
		String message_forgotpassword = "";
		if(Checks.isEmpty(username)||Checks.isEmpty(sq)||Checks.isEmpty(sans)){
			message_forgotpassword="Please fill all fields.";
		}else{ 
			LoginInfo data = LoginInfoDAO.checklogin(username);
			if(data!=null){
				if(data.getSq().equals(sq) && data.getSans().equals(sans)){
					char[] resetpass = RandomString.generatePswd(12, 20, 5, 3, 4);
					String resetpassword = new String(resetpass);
					String messageText = "<h3> Your Reset Password is : " + resetpassword + "  </h3>";
					String subject = "Password Reset";
					if(Utility.SendEmail(data.getEmailid(), subject, messageText)){
						data.setPassword(resetpassword);
						LoginInfoDAO.updatelogin(data);
						message_forgotpassword = "Password Reseted and Send to Your Registered Email ID.";
						//message+= " Reseted password is: "+resetpassword;
					}else{
						message_forgotpassword="Internet is not working...Password is not reset.";
					}
				}else{
					message_forgotpassword="invalid security question/ answer :-- Password is not reset.";
				}
			}else{
				message_forgotpassword="username is not valid. :--  Password is not reset.";
			}
		}
		
		request.setAttribute("message_forgotpassword", message_forgotpassword);
		RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
			rd.forward(request,response);
	}

}
