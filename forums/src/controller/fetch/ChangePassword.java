package controller.fetch;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.dao.LoginInfoDAO;
import model.to.LoginInfo;

import operations.Checks;

@WebServlet("/changepassword.do")
public class ChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String oldpassword = request.getParameter("oldpassword") != null ? request
				.getParameter("oldpassword") : "";
		String newpassword = request.getParameter("newpassword") != null ? request
				.getParameter("newpassword") : "";
		String newpassword1 = request.getParameter("newpassword1") != null ? request
				.getParameter("newpassword1") : "";
		String username = "";
		HttpSession session = request.getSession();
		String message_changepassword = "";		

		if (Checks.isEmpty(oldpassword) || Checks.isEmpty(newpassword)
				|| Checks.isEmpty(newpassword1)) {
			message_changepassword = "Please fill all fields.";
		} else {
			if(session.getAttribute("auname")!=null){
				username = session.getAttribute("auname").toString();
			}else if(session.getAttribute("uname")!=null){
				username = session.getAttribute("uname").toString();
			}
			LoginInfo data = LoginInfoDAO.checklogin(username);
			if(data != null){				
				if(data.getPassword().equals(oldpassword)){
					if(newpassword.equals(newpassword1)){
						data.setPassword(newpassword);
						if(LoginInfoDAO.updatelogin(data)){							
							message_changepassword = "Password change succesfully.";
						}else{
							message_changepassword = "Password doesnot chnage.";
						}
					}else{
						message_changepassword = "Password doesnot match.";
					}
				}else{
					message_changepassword = "Old Password is not correctly entered.";
				}
			}else{
				message_changepassword = "Invalid User.";
			}
		}

		request.setAttribute("message_changepassword", message_changepassword);
		RequestDispatcher rd = request
				.getRequestDispatcher("changepassword.jsp");
		rd.forward(request, response);
	}
}
