package controller.fetch;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.dao.ThreadInfoDAO;
import model.to.ThreadInfo;

@WebServlet("/getallthreads.do")
public class GetAllThreads extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String message_getallthreads = "";
		
		List<ThreadInfo> allthreads = ThreadInfoDAO.getallrecordOrderByThreadId(0); 
		request.setAttribute("allthreads", allthreads);
		request.setAttribute("message_getallthreads", message_getallthreads);
		RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
		rd.forward(request, response);
	}

}
