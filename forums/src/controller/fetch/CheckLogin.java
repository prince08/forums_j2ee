package controller.fetch;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.LoginInfoDAO;
import model.dao.MemberInfoDAO;
import model.to.LoginInfo;
import model.to.MemberInfo;
import operations.Checks;

/**
 * Servlet implementation class CheckLogin
 */
@WebServlet("/checklogin.do")
public class CheckLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("username") != null ? request
				.getParameter("username") : "";
		String password = request.getParameter("password") != null ? request
				.getParameter("password") : "";
		boolean flag = true;

		if (Checks.isEmpty(username) || Checks.isEmpty(password)) {
			request.setAttribute("message", "Please fill all fields");
		} else {			
			LoginInfo data = LoginInfoDAO.checklogin(username);
			if (data != null) {				
				if (data.getPassword().equals(password)) {
					HttpSession session = request.getSession();
					if (data.getRolename().equalsIgnoreCase("admin")) {
						
						session.setAttribute("auname", username);						
						session.setAttribute("lastlogin", data.getLastlogin());
						data.setLastlogin(new java.util.Date());
						flag = false;
						response.sendRedirect("admin/index.jsp");
					} else if (data.getRolename().equalsIgnoreCase("member")) {
						MemberInfo data1 = MemberInfoDAO.getRecords(username);
						session.setAttribute("memberid", data1.getMemberid());
						
						session.setAttribute("uname", username);											
						session.setAttribute("lastlogin", data.getLastlogin());
						data.setLastlogin(new java.util.Date());
						flag = false;
						response.sendRedirect("member/index.jsp");
					} else {
						request.setAttribute("message", "Invalid role..");
					}
				} else {
					request.setAttribute("message", "Invalid password.");
				}
			} else {
				request.setAttribute("message", "Invallid username.");
			}
		}
		if (flag) {
			RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
			rd.forward(request, response);
		}
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request,response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request,response);
	}

}
