package controller.fetch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DownloadPic
 */
@WebServlet("/member/downloadpic.do")
public class DownloadPic extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String picname = request.getParameter("picname");
		String path = getServletContext().getRealPath("/upload/"+picname);		
		if(path != null && picname != null){
			File file = new File(path);
			if(file.exists()){
				response.reset();
				response.setContentType("image/jpeg");
					FileInputStream fis = new FileInputStream(file);
				response.setContentLength(fis.available());
				response.addHeader("content-disposition", "attachment;filename=" + picname);
				byte[] data = new byte[fis.available()];
				fis.read(data);
				fis.close();
				response.getOutputStream().write(data);
			}else{
				response.sendRedirect("addmemberpic.jsp");
			}
		}else{
			response.sendRedirect("addmemberpic.jsp");
		}
	}
	
}
