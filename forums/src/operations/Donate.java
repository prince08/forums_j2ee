package operations;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/donate.do")
public class Donate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_cart&upload=1&business=mdm1@gmail.com";
		String successURL = "http://localhost:5656/forums/home.jsp?err=success";
		String failureURL = "http://localhost:5656/forums/home.jsp?err=fail";

		url += "&item_name_1=Donation Amount&amount_1=10";

		url += "&currency=USD";
		url += "&return=" + successURL;
		url += "&cancel_return=" + failureURL;
		response.sendRedirect(url);

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

}
