package operations;

public class Checks {
	public static boolean isEmpty(String value){
		return value.trim().equals("");
	}
	public static boolean isNumeric(String value){
		try{
			Long.parseLong(value);
			return true;
		}catch(NumberFormatException ex){
			return false;
		}
	}
}
