package operations;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Contact
 */
@WebServlet("/contact.do")
public class Contact extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message_contact = "";
		String name = request.getParameter("name") != null ? request
				.getParameter("name") : "";
		String email = request.getParameter("email") != null ? request
				.getParameter("email") : "";
		String contactno = request.getParameter("contactno") != null ? request
				.getParameter("contactno") : "";
		String subject = request.getParameter("subject") != null ? request
				.getParameter("subject") : "";
		String message = request.getParameter("mess") != null ? request
				.getParameter("mess") : "";

		if (Checks.isEmpty(name) || Checks.isEmpty(subject)
				|| Checks.isEmpty(message) || Checks.isEmpty(contactno)
				|| Checks.isEmpty(email)) {
			message_contact = "Please fill all fields complety.";
		} else {
			String messagetext = "<h3>" + name + "</h3><h4>" + subject
					+ "</h4><p>" + message + "</p>"+"<p>Email Id: "+email+"</p><p>Contact Number: "+contactno+"</p>";
			if (Utility.SendEmail("prince2108.pv@gmail.com", subject,
					messagetext)) {
				//System.out.println(messagetext);
				message_contact = "Your message is send to our team. Your request will be processed soon.";
			} else {
				//System.out.println(messagetext);
				message_contact = "Your Internet connecttion is not working properly.";
			}
		}
		request.setAttribute("message_contact", message_contact);
		RequestDispatcher rd = request.getRequestDispatcher("contact.jsp");
		rd.forward(request, response);
	}

}
